# Présentation du projet

Bienvenu sur ma page projet lié au cours de fabrication numérique "How To Make (almost) Any Experiment Using Digital Fabrication" donné par Denis Terwagne à l'ULB.

Pour ce projet, je me suis intéressé à la Fast Fashion et aux problèmes qui l'entoure, et en particulier, j'ai étudié la possibilité de décolore du denim par simple exposition au soleil. Pour cela j'ai réalisé une boite qui permet de protéger le tissu à l'exception de la partie qu'on souhaite décolorer. Le design de la boite permet de choisir un motif qu'on souhaite faire apparaître par décoloration sur le vêtement.

#### Avant décoloration
![image](../images/project/concept1.jpg "proof of concept 1")

#### Après décoloration
![image](../images/project/concept2.jpg "proof of concept 2")

# Naissance du projet : Une préoccupation, une question, une idée.
Quand après les six semaines de formation aux outils du Fablab il a été question du projet, Denis nous a tout de suite poussé à réfléchir à quelque chose qu'on _**voulait**_ faire plutôt qu'à quelque chose qu'on _**pouvait**_ faire. Spontanément, je pense que j'aurais regardé les outils disponibles aux Fablab, et réfléchi à ce que je pouvais faire avec, jusqu'à trouver une idée qui me semble satisfaisante. Ce que Denis nous a demandé de faire, c'est d'oublier le Fablab, et de juste nous concentrer sur des choses qui nous préoccupaient, des idées, sujets de sociétés qui étaient importants pour nous.

Parmi les choses qui me semblent importantes aujourd'hui, j'ai choisi de travailler sur l'industrie de la mode et en particulier sur la **Fast Fashion**.

## C'est quoi la Fast Fashion ? ##
La Fast Fashion est un modèle de production et de vente de vêtements ayant émergé dans les années 70 dont le maître mot est **"vendre plus pour moins cher"**.
Vous avez sans doute déjà entendu parler de certaines des plus grosses entreprises de fast fashion  **_Zara_, _Primark_, _H&M_**, mais ces entreprises sont déjà concurrencées par l'_Ultra-Fast Fashion_ dont je reparlerai en fin de section.

Là où les marques "régulières" ont l'habitude de proposer une collection hiver et une collection été, les entreprises de Fast Fashion renouvellent en permanence leur offre, proposant chaque semaine de nouveaux modèles. Les vêtements apparaissent et disparaissent des vitrines, ce qui crée un effet de rareté sur les produits ; si vous voyez un modèle qui vous plait, vous n'avez pas le temps de vous demander si vous le voulez vraiment, si vous ne l'achetez pas maintenant, vous l'aurez raté à jamais. Cet effet, couplé au prix de vente très bas, favorise les achats compulsifs.

Pour pouvoir vendre les vêtements à bas prix, les entreprises doivent diminuer au maximum les coûts de production, ce qui a **des conséquences graves**, directes et indirectes **sur les ouvriers, les populations situées près des usines et sur l'environnement**.

En premier lieu, pour produire plus de vêtements, il faut plus de matières premières, qu'elles soient naturelles (coton, lin, laine, etc.) ou de synthèse (dérivés de produits pétrolier : viscose, nylon, polyester, acryliques, élasthanne), sans parler de tous les produits utilisés pendant la fabrication. Augmenter la production de vêtements, c'est mécaniquement augmenter la pollution qui y est associée.

D'un point de vue de la conception, les modèles sont des copies de modèles de haute-couture, modifiés juste assez pour éviter les accusations de vol.

Les entreprises de Fast Fashion ont délocalisé les usines et atelier qui fabriquent les tissus et vêtements dans des pays où les salaires sont bas (principalement en Asie : Inde, Bengladesh, Chine, Indonésie.) et où les autorités sont également moins regardantes sur les normes environnementales et sur les droits des travailleurs.
Cela permet aux entreprises de couper les coûts liés au traitement des déchets. Non traités, ces déchets finiront dans les eaux et les sols environnants, rendant plus difficile l'agriculture et l'accès à l'eau aux populations locales, et pouvant également être source de maladies.
Les personnes travaillant dans les usines sont encore plus exposées aux produits dangereux qui y sont utilisés et ne disposent souvent pas de matériel de protection adéquat.
À ce manque de protection au sens propre s'ajoute le manque de protection au sens figuré : les ouvriers peuvent être licencié du jour au lendemain sans contrepartie, avec des horaires irrégulier mais pouvant dépasser les 14h par jour et travaillent pour des salaires qui leurs permettent à peine de survivre. De nombreux cas de travail d'enfants ont également été dénoncés. On peut sans exagération parler d'esclavage moderne, car bien que les travailleurs ne soient pas considérés comme la propriété d'une personne, leurs conditions de vie ne leurs laissent pas d'autres perspectives que celle de travailler chaque jour pour voir le lendemain.
Une autre source de pollution est le transport des vêtements à travers le monde, des usines de textile aux ateliers de couture puis aux magasins, autre conséquence de la délocalisation des usines et ateliers.
Enfin les vêtements sont de moindres qualités, s'usent rapidement, et combiné avec le besoin crée chez les consommateurs de renouveler sans cesse leur garde-robe, une quantité énorme de vêtements sont jetés chaque année. Ces vêtements sont souvent fabriqués avec des textiles de si mauvaise qualité que leur recyclage est impossible, et les centres de tri sont submergés par la quantité qui y arrive.

L'industrie de la fast fashion évolue, et malheureusement pas pour le meilleur. Un nouveau modèle a émergé depuis quelques années, qualifié d'Ultra-Fast Fashion. Avec ce modèle, les entreprises telles que **_Boohoo_, _ASOS_** ou **_Missguided_** proposent encore plus de modèles, de la vente exclusivement en ligne, et une communication axée vers les adolescents et jeunes adultes via les réseaux sociaux et les influenceurs (_Facebook_, _Instagram_, _Tik Tok_, etc.)

Pour plus de détails, je vous recommande la lecture (en anglais) de l'article "[What's wrong with the fashion industry?](https://www.sustainyourstyle.org/en/whats-wrong-with-the-fashion-industry?gclid=CjwKCAjwxuuCBhATEiwAIIIz0bR7hXXgyCsgSD9Xmq8WykgCq848GOz_Gs3AcQrV9gY9K_Ou0wfV4BoCHqcQAvD_BwE)"

En plus de toutes ces raisons qui, je pense, suffisent à convaincre de l'importance de la problématique, j'ai souhaité travailler autour de ce sujet car au plus j'en apprend, au plus je suis dérangé par le déséquilibre entre son importance et son absence des discussions.

Les vêtements font partie des biens les plus fondamentaux que nous devons fabriquer. Jusqu'à la fin du XVIIIe siècle, la production de vêtement était lente et couteuse, réservant l'accès aux vêtements de qualité aux plus fortunés.
Puis, avec la révolution industrielle et la mécanisation de l'industrie textile, il est devenu beaucoup plus facile de fabriquer des tissus, et une part croissante de la population a eu accès à ces produits. Cette amélioration s'est faite au détriment des ouvriers travaillant dans les usines, ce fût le cas partout et donc aussi ici, en Belgique.
Aujourd'hui, avec la mondialisation des systèmes de productions, ce schisme se répète : une part de la population a désormais un accès facile aux vêtements au détriments des personnes les fabriquant, seulement cette fois, les populations sont séparées géographiquement, ce qui facilite l'invisibilisation de cette réalité.

## Formuler une question
Maintenant que le problème est identifié, l'objectif était de parvenir à formuler une question qui en contiendrait l'essence et à laquelle trouver la réponse me permettrait de définir un projet.
Je suis arrivé à **"Comment produire des vêtements humainement et écologiquement responsables, à des coûts assez bas pour que les consommateurs les choisissent face à des productions Fast Fashion ?"**. J'ai eu l'occasion d'échanger avec les autres personnes suivant le cours, et leurs questions sont venu nourrir ma réflexion :

![Question et questions des autres](../images/project/questions.jpg "ici")

Grâce à ces échanges j'ai pu dégager plusieurs sous questions :

1. Comment communiquer les problèmes liés à la fast-fashion ?
2. Quels sont les standards que doit satisfaire la chaîne de production ?
3. Quel(s) produit(s) veut-on proposer ?
4. Pourquoi le consommateur choisirait ce produit ?
5. Quelle est la place et l'impact d'une telle initiative dans l'économie à plusieurs milliers de milliards de US $ de la mode ?

## Réduction à un problème précis
Je n'ai pas trouvé de réponse à toutes ces questions, mais elles m'ont permis de continuer à réfléchir. Je devais encore réduire la question pour arriver à quelque chose que je pouvais traiter dans le cadre du cours, et j'ai choisi de me concentrer sur la chaîne de production des jeans.

Les principales étapes de la production d'un jeans ou d'une veste en denim sont les suivantes :

1. La culture du coton
2. Le filage du coton brut
3. La teinture des fils
4. Le tissage des fils de coton
5. La découpe
6. Les traitements post-assemblage
7. La distribution pour la vente

Encore une fois, il y a ici plein de problèmes auxquels on pourrait s'atteler, j'ai décidé d'investiguer une des étapes de traitement des vêtements assemblés, **le délavage**.

Le délavage est l'étape consistant à user et décolore le jeans, parfois de façon uniforme, mais le plus souvent de manière localisée, afin de lui donner un aspect usé.
Il existe plusieurs techniques permettant d'y arriver, (utilisation de produits chimique, lavage avec des pierres, sablage, décoloration laser et autres)  mais toutes celles assez rentables pour être utilisées en industries sont soit polluantes, soit dangereuse pour les ouvriers, soit les deux.

Je suis tombé sur un article, "Photocatalytic discoloration of denim using advanced oxidation processwith H2O2/UV" par Izadyar Ebrahimi, Mazeyar Parvinzadeh Gashti et Mojtaba Sarafpour, des chercheurs basés au Canda et en Iran. Ils sont parvenus à décolorer du denim en le lavant avec du peroxyde d'hydrogène puis en l'exposant à de la lumière ultra-violette.
Voilà une photo du résultat :
![ebrahimi résultats denim](../images/project/EbrahimiPhotosResultats.jpg)
(a) le tissu non traité, (b) lavé au peroxyde d'hydrogène, (c-g) lavé et exposé à des UV pendant 30 à 150 minutes.

Cette réussite m'a motivé à essayer à mon tour de délaver un jeans avec des UV !
Mais, comment ça marche ?



# La décoloration par la lumière #

Vous avez sans doute déjà remarqué un fauteuil, une vieille photo, des tentures ou même des vêtements qui ont perdus de leur éclat. Cela peut être d'autant plus flagrant si, par exemple, une fenêtre laisse passer le soleil mais que seule une partie de l'objet en question se retrouve en pleine lumière. Mais pourquoi la lumière attaque-t-elle les couleurs ?

### Lumière et couleurs
Ce que nous appelons couleur, c'est la façon qu'a notre cerveau de traduire une des propriétés de la lumière : sa longueur d'onde (ou de façon équivalent sa fréquence, son énergie). Ainsi, les humains sont capables de percevoir la lumière dont la longueur d'onde est comprise entre 380 et 750 nanomètres (ce sont des estimations, ces limites varient d'un individu à l'autre et selon les conditions d'observation.)
Il est important de noter qu'au plus la longueur d'onde est petite, au plus l'énergie portée par chaque photon est grande. Ainsi la lumière ultra-violette, dont les longueurs d'onde vont d'environ 10 nm à 400 nm, transporte plus d'énergie que la lumière visible.

Maintenant qu'est ce qui fait qu'un objet nous apparaitra d'une certaine couleur ?
La couleur peut avoir deux origines :

1. La composition chimique de l'objet : Chaque atome ou molécules absorbe certaines longueurs d'ondes (certaines couleurs) et réfléchi les autres.
2. La structure de l'objet : Lorsque la lumière rencontre une surface dont les défauts ont une taille du même ordre de grandeur que la lumière, il peut arriver que certaines longueurs d'onde disparaissent au profit d'autres. C'est par exemple le cas chez certains papillons appartenant au genre _morpho_ dont la couleur bleue métallique est due à la structure des écailles couvrant leurs ailes.

![papillon bleu par structure](../images/project/papillon3.jpg)

Les couleurs par structure sont rares dans la nature, et en particulier les teintures doivent leurs couleurs à leur composition chimique.
La teinture utilisée pour donner leur couleur bleue aux jeans est l'indigo, une molécule qu'on trouve à l'état naturel chez certaines plantes du genre _indigofera_, mais dont la production par synthèse est aujourd'hui largement majoritaire.

##### Brique de teinture indigo :
![indigo](../images/project/indigo3.jpg)

##### Molécule d'indigo :
![molécule d'indigo](../images/project/indigoMolecule4.jpg)


### La décoloration
La teinture à base d'indigo n'interagit pas fortement avec les fils de coton, c'est à dire que les liens créés entre les deux lors de la teinture ne sont pas très solide, et qu'ils peuvent facilement être cassé par friction. C'est ce qui se passe lors du délavage par sablage, par lavage avec des pierres ou par usure naturelle tout simplement. Mais ce n'est pas la cause de décoloration qui nous intéresse ici.

Si la couleur d'une teinture tient à sa composition chimique, alors un changement de couleur peut être obtenu par un changement dans cette même composition. Lorsque ce changement est obtenu grâce à de la lumière (en particulier de la lumière visible ou ultraviolette), on parle de **Photodégradation**.
Il existe plusieurs types de photodégradation, selon la présence ou l'absence d'autres réactifs chimiques :

* Lorsqu'un photon est absorbé par la molécule et que sa seule énergie suffit à casser cette dernière en deux (ou plus) nouvelles molécules, on parle de **photolyse**.

* Il arrive aussi que la molécule absorbe un photon sans pour autant se décomposer. On dit alors que la molécule est dans un état excité. Si la molécule est isolée, elle finira par réémettre le photon pour retourner dans son étant fondamental. Mais tant qu'elle est dans son état excité, la molécule est beaucoup plus réactive, en particulier avec l'oxygène présent dans l'atmosphère, et de la même façon que le métal rouille, elle subit alors une dégradation, la **photo-oxidation**.

* Un processus similaire peut se produire avec des molécules d'eau au lieu de l'oxygène, la **photo-hydrolyse**.

Dans leur article, Ebrahimi et ses collègues proposent le mécanisme de décomposition suivant :
![processus de dégradation proposé par Ebrahimi](../images/project/EbrahimiProcess.jpg)
Le peroxyde d'hydrogène (\(H_2O_2\)) subit une photo-oxydation et se décompose en molécules \(OH\) et \(OOH\). Ces molécules, toujours avec l'aide de la lumière, peuvent alors réagir avec les molécules d'indigo pour les décomposer. (Ces nouvelles molécules vont à leur tour être décomposées en plus petites molécules mais ce qui nous intéresse surtout c'est comment on passe d'une molécule d'indigo, bleue, à d'autres molécules).

# Tests et design

<p class="text-muted">
<b>Remarque : </b> Dans cette section, je vais parfois passer d'un point à l'autre avant de revenir au précédent. J'espère que ça restera clair pour vous, le but est de restituer le cheminement qui a été le mien au cours des dernières semaines.
</p>

Maintenant qu'on a une idée de comment la lumière peut nous permettre de décolorer un jeans, il est temps de faire des tests et de réfléchir à un projet concret.
Dès le départ je ne voulais pas me contenter d'explorer l'aspect technique, je voulais aussi réfléchir à l'aspect communication et utilisation ; Je voulais créer un dispositif permettant de délaver un jeans proprement et qui ferait dire aux gens "**Hé c'est cool ! La prochaine fois que j'achèterai un jeans j'en prendrai un pas décoloré et j'utiliserai cette technique pour le faire moi-même.**"
Je pense que dire aux gens qu'une chose qu'ils font est mauvaise pour l'environnement ou les travailleurs, ça marche moyen. À mons avis, quelque chose qui pourrait mieux marcher, en particulier chez les personnes qui achètent beaucoup de vêtements et qui se préoccupent de leur style, ça serait de proposer une technique qui en plus d'être écologique et responsable, permettent de rendre les jeans qu'on décolore unique.
Et justement, le fablab dispose d'une chose qui permet ça : le _Computer-Aided Design_ (CAD), la conception assistée par ordinateur. Au cours du quadrimestre nous avons appris à utiliser des logiciels de dessins 2D et 3D, afin de créer des objets en impression 3D ou avec des découpeuses laser, découpeuses à lame. La précision obtenue avec ces machines dépasse de loin ce qu'on peut atteindre à la main.

J'avais donc mon concept : _proposer une méthode de délavage de jeans écologique et responsale **et** qui permette de personnaliser son vêtement en le décolorant selon un dessin, un motif, un message qu'on a envie de porter._

Il y a donc deux parties :

1. Trouver comment utiliser la conception assistée par ordinateur pour décolorer selon un dessin.
2. Tester la décoloration par UV.

Pour la première partie, j'ai instinctivement pensé à transposer ce que je voyais autour de moi : Les imprimantes 3D et les découpeuses laser on des têtes d'impression / de découpe qui peuvent se déplacer selon deux ou trois axes et ainsi reproduire le dessin envoyé à la machine via ordinateur.
Je pourrais utiliser un de ces systèmes pour déplacer une lampe dont la lumière serait concentrée sur une petite zone du tissu, ne décolorant que cette partie-là. Avec un système composé de deux axes, on pourrait ainsi reproduire n'importe quel dessin 2D sur le vêtement.
C'est une idée qui me plait beaucoup, et je vais mettre un peu de temps à me rendre compte d'un gros souci. Mais avant d'en parler, passons un moment à la seconde partie.

Tester la décoloration de jeans par exposition à de la lumière UV nécessite de faire un choix : quel type d'UV utiliser ?
En effet, comme mentionné plus haut, la lumière UV va des longueurs d'onde de 10 à 400 nanomètres, mais ce spectre en subdivisé en trois catégories, selon leurs propriétés :

<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">longueurs d'onde (nm)</th>
      <th scope="col">présence à la surface de la terre</th>
      <th scope="col">Effets et dangers</th>
    </tr>
  </thead>
  <tbody>
    <tr class="table-dark">  <!-- thick -->
      <th style="text-align: center" scope="row">UVA</th>
      <td style="text-align: center" >315 - 400</td>
      <td>Très peu absorbés par l'atmosphère, la majorité des UVA émis par le soleil atteignent la surface de la terre.</td>
      <td> Sans danger, ce sont ces UV qui sont émis par les lampes à lumière noire, utilisée pour faire s'éclairer des matières fluorescentes ou vérifier l'authenticité de billets de banque. </td>
    </tr>
    <tr class="table-dark">  <!-- rOut -->
      <th style="text-align: center" scope="row">UVB</th>
      <td style="text-align: center" >280 - 315</td>
      <td>Fortement absorbés par l'atmosphère, une petite fraction des UVB émis par le soleil nous atteignent</td>
      <td> Ce sont ces UV qui nous font bronzer (ou qui nous donnent des coups de soleils !). </td>
    </tr>
    <tr class="table-dark"> <!-- rInn -->
      <th style="text-align: center" scope="row">UVC</th>
      <td style="text-align: center" >100 - 280</td>
      <td>Complètement absorbé par l'atmosphère. </td>
      <td> Très dangereux, ces UV sont capable de casser les molécules d'ADN. Pour cette raison des lampes germicides, emettant des UVC, ont été fabriquées. Elles sont utilisés pour nettoyer surfaces et objets des bactéries et virus présents dessus. </td>
    </tr>
  </tbody>
</table>

J'ai fait un premier test, en utilisant le plateau d'une imprimante 3D fonctionnant avec des UVB. J'ai trempé deux morceaux de jeans dans une solution de peroxyde d'hydrogène 30%, diluée respectivement 3 fois et 10 fois. J'ai placé ces échantillons sur le plateau de l'imprimante avec un échantillon sec, et je les ai laissés deux heures et demie, la même durée que pour l'expérience réalisée par Ebrahimi et ses collègues. Dans le plateau se trouve des lampes UVB, j'ai placé une boite par dessus les échantillons en guise de protection.
Au bout de ce temps, je n'ai remarqué aucune différence notable. Je les ai donc laissés jusqu'au lendemain, pour un total d'environ 20h, et là il y avait bien une différence.
Les échantillons laissé sur le plateau sont significativement moins colorés, indépendamment de la présence de peroxyde d'hydrogène, indiquant que c'est surtout la lumière qui est responsable de la décoloration ici.

##### Avant le test
![échantillons lavé](../images/project/uvb1.jpg)
##### Plateau de l'imprimante avec protection
![résultat après une nuit sur la lampe à UVB](../images/project/uvb.jpg)
##### Après 20h d'exposition
![résultat après une nuit sur la lampe à UVB](../images/project/uvb2.jpg)

Un résultat, mais dont je n'étais pas vraiment satisfait. Le processus est trop lent, si chaque pièce doit être exposé une vingtaine d'heures, monopolisant pendant ce temps la lampe, ça me parait difficilement applicable en dehors d'un laboratoire.

Dans leur expérience, Ebrahimi et ses collègues ont utilisé une lampe à mercure, qui émet uniquement des UVC à 253.7 nanomètres, je pense donc me tourner vers cette gamme de lumière, il y a justement au fablab une lampe du même genre que celle utilisée dans leur expérience. Mais se pose alors la question de la sécurité, les UVC ne doivent pas être manipulés à la légère. Je commence donc à me documenter sur ce qui permet de bloquer cette partie du spectre et quels sont les équipements de protection adéquat.

Revenons un instant à la première partie, sur la façon dont on pourrait décolorer selon un motif bien précis. J'ai beau aimer l'idée de placer la lampe sur une tête pouvant se déplacer, je me rends compte qu'il y a un moyen bien plus simple d'arriver au même résultat : Plutôt que de chercher à contrôler précisément la direction et la position de la lampe,  il suffit en fait de laisser la lampe éclairer librement, et de se contenter de bloquer la lumière là où on ne veut pas de décoloration, en couvrant le tissu. Donc essentiellement, ce dont j'ai besoin, c'est un pochoir, et ça tombe bien, je sais déjà le faire avec la découpeuse laser !

Ainsi, en parallèle de la recherche sur la protection aux UVC, je réfléchi à un dispositif sommaire : une boite hermétique dans laquelle on place la lampe UV et le tissu dont on n'expose que la partie qu'on souhaite délaver.
![schéma moche](../images/project/schemaMoche.jpg)

Il s'avère que c'est très dur de trouver de la documentation claire et fiable sur ce qui bloque ou non les UVC. Pour la protection personnelle, ça va, il est très clair que pas un centimètre carré de peau ne doit être exposé, et en particulier le visage et les yeux doivent être protégé.
<p class="text-muted">
<b>Remarque : </b> C'est pour ce protéger des UVC que les soudeurs à arc portent un tel équipement, j'ai essayé de contacter des ateliers à bruxelles et le FabLab VUB pour voir s'ils pouvaient m'aider, mais sans succès, je pense que la situation covid n'a pas aidé.
</p>

![un soudeur à arc bien protégé](../images/project/soudeurArc4.jpg)

Pour ce qui est du caisson, les documentations officielles recommandent de prendre toutes les mesures de protection nécessaire, sans spécifier ce qu’elles sont. Je suis tombé sur plusieurs documents de ce genre et je me demande si ce n'est pas une manière pour eux d'éviter les poursuites si quelqu'un a un accident après avoir suivi leurs recommandations.


Un autre problème, est qu'avec le covid, il y a un pic d'intérêt pour les lampes germicide, et qu'il y a beaucoup de documentation amateure, sans sources, pour des projets DIY type "_comment faire une boite germicide pour nettoyer vos masques !_"

Alors que je galère à préparer l'expérience, je me rends compte d'une chose : Si moi-même, qui suis au cœur du projet, j'ai tant de mal à mettre en place un dispositif, quelles sont les chances pour que d'autres personnes aient envie de reproduire l'expérience ensuite ? Se pose également la question de la frugalité, même si on a la motivation nécessaire, trouver une lampe UVC et l'équipement requis pour s'en protéger ce n'est pas du tout évident.

Je décide donc d'effectuer un virage "_Radical Frugal_", et de ne garder que l'essentiel. Pour la lumière, ça sera celle du soleil. Ça prendra du temps, mais tout le monde y a accès (pas de blague sur la météo belge).
Je lance alors une série de test pour voir si certains produits ménager courant peuvent aider la décoloration. J'accroche dehors un panneau avec des échantillons de jeans que je viens régulièrement imbiber de différents produits :

##### Avant exposition au soleil :
![panneau début](../images/project/panneau1.jpg)

##### 15 jours plus tard :
![panneau fin](../images/project/panneau2.jpg)

Vous aurez peut-être remarqué l'apparition d'un 6e échantillon non libellé, il s'agit d'un échantillon traité avec de l'eau salée. C'est Jason, avec qui j'ai discuté au Fablab qui m'a conseillé d'essayer ça. Du coup cet échantillon n'est resté que quelques jours au soleil et je pense que ça vaudrait la peine de poursuivre l'expérience.

Quand j'ai commencé ce test, on était à deux semaines de la fin du projet, et je savais que je risquais de ne pas voir d'effet en si peu de temps. Et en effet, je n'ai d'abord vu aucun résultat. Lorsque j'ai lavé les échantillons par contre, j'ai vu que celui que j'avais traité avec de la lessive présentait une décoloration visible. Mais malheureusement, j'ai appris entre temps que contrairement aux autres produits que j'ai testés, la lessive pose un vrai problème de pollution des eaux, et je ne peux donc pas le recommander comme accélérateur.

##### échantillon avec lessive après rinçage :
![panneau lessive](../images/project/panneauLessive2.jpg)


Il reste à concevoir la façon dont on expose et protège les différentes parties du tissu.
 Je fais d'abord le brouillon d'une boite dont une face, inclinée à 45°, est composée de deux couche : En dessous un plateau, qui permet de soutenir le tissu, et au-dessus le couvercle/pochoir, pour décolorer selon notre dessin.
![](../images/project/boiteSchema.jpg)

Ensuite, avec le logiciel FreeCAD, je dessine les différentes pièces de la boite. FreeCAD permet de dessiner en paramétrisant les longueurs du dessin à partir d'une feuille de donnée, ce qui rend le modèle très facile à adapter, il suffit de changer une grandeur dans la feuille pour que tout le dessin s'adapte.
![FreeCAD](../images/project/boiteFreeCAD.jpg)
De haut en bas et de gauche à droite : le support, le côté de la boite (à découper deux fois), l'arrière de la boite, le couvercle (ici sans motif à décolorer).

<p class="text-muted">
Les fichiers FreeCAD ainsi que les dessins vectoriels envoyé à la découpeuse laser sont disponibles sur le _repository_ GitLab (<a href="https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/tree/master/docs/projet/CAD%20files">ici</a>)</p>

Vous avez déjà vu la boite _proof of concept_ en haut de page, voilà la première boite assemblée, en papier. Il n'y a pas de motif sur celle-ci, je voulais juste tester les dimensions :
![](../images/project/boitePapier2.jpg)

# Bilan

D'un point de vue technique, je ne doute pas de l'efficacité du dispositif, en exposant assez longtemps la boite au soleil on obtiendra bien le résultat attendu, nos parents et grands-parents n'ont pas attendu les Fablab pour savoir que faire sécher sa lessive au soleil c'est une mauvaise idée.

Mais le fait que le processus soit lent rend difficile sa diffusion. Je pense que ça peut intéresser des gens déjà sensibilisés aux problématiques environnementales et liées à l'industrie de la mode. En revanche, je ne pense pas que ça attire des personnes qui ont l'habitude d'acheter beaucoup de vêtements et de les jeter tout aussi rapidement.

Cependant, pour les personnes plus intéressées par l'aspect mode qu'écologie, je pense qu'il y a d'autres portes d'entrées vers la bulle _makers_ et Fablab. Par exemple, les découpeuses laser fonctionnent très bien avec certains textiles et on peut facilement imaginer découper et coudre des patchs pour personnaliser des vêtements.

De façon plus général, mener à bien ce projet m'a fait découvrir l'intersection entre les univers des _makers_ et du textile et je pense qu'il y a énormément de choses à explorer.

Si ça vous intéresse et que vous êtes près de Bruxelles, je ne peux que vous recommander d'aller voir Valentine Fruchart à la [Green Fabric](https://greenfabric.be/).
Et où que vous soyez, la [Fabricademy](https://textile-academy.org/) propose des formations et beaucoup de documentation sur la place de la fabrication numérique dans l'industrie textile.

D'un point de vue personnel, c'est la première fois que je suis amené à réaliser un projet de ce genre, et j'en ai tiré plusieurs leçons:

1. Une personne, en un temps donné, est forcément limité dans ce qu'elle peut faire. Il faut donc savoir faire le deuil de certaines idées, ou au moins accepter que certaines idées sortent du cadre du projet et qu'il faut les garder pour plus tard.

2. En parlant d'idées à garder et d'idée à jeter, même si on a une idée qui nous plait vraiment, il faut savoir l'abandonner si elle ne fonctionne pas ou s’il y a moyen de faire plus simple (même si c'est moins spectaculaire, _cf._ "une lampe UV sur qui vise un point précis et se déplace selon un dessin" vs "un pochoir exposé au soleil", techniquement impressionnant ne veut pas dire intelligent).

3. Faire des recherches c'est bien, c'est même crucial, mais ce qui m'a fait le plus avancer au final c'est discuter avec des gens autour de moi, qu'ils soient impliqués dans l'écosystème Fablab ou pas.
 ![c'est ça la puissance intellectuelle](../images/project/puissance.jpg)


# Sources #

### Fast Fashion
* [What's wrong with the fashion industry?](https://www.sustainyourstyle.org/en/whats-wrong-with-the-fashion-industry?gclid=CjwKCAjwxuuCBhATEiwAIIIz0bR7hXXgyCsgSD9Xmq8WykgCq848GOz_Gs3AcQrV9gY9K_Ou0wfV4BoCHqcQAvD_BwE)
* [Fashion Is Not the 2nd Most Polluting Industry After Oil. But What Is It? ](https://ecocult.com/now-know-fashion-5th-polluting-industry-equal-livestock/)
* [Ultra-fast Fashion Is Eating the World](https://www.theatlantic.com/magazine/archive/2021/03/ultra-fast-fashion-is-eating-the-world/617794/)
* [Child labour in the fashion supply chain](https://labs.theguardian.com/unicef-child-labour/)
* [The Stat of Fashion 2020, _McKinsey & Company_](https://www.mckinsey.com/~/media/mckinsey/industries/retail/our%20insights/the%20state%20of%20fashion%202020%20navigating%20uncertainty/the-state-of-fashion-2020-final.pdf)
* [Fashion’s impact in numbers](https://edition.cnn.com/interactive/2020/09/style/fashion-in-numbers-sept/)
* Wikipedia : [Fast Fashion](https://en.wikipedia.org/wiki/Fast_fashion), [Fashion](https://en.wikipedia.org/wiki/Fashion), [Clothing Industry](https://en.wikipedia.org/wiki/Clothing_industry), [Industrial revolution](https://en.wikipedia.org/wiki/Industrial_Revolution)

### Couleur et décoloration

* [Ebrahimi et al., _Photocatalytic discoloration of denim using advanced oxidation process with H2O2/UV_](https://www.sciencedirect.com/science/article/abs/pii/S1010603018303885)
* [General introduction to the chemistry of dyes](https://www.ncbi.nlm.nih.gov/books/NBK385442/)
* [Couleur induite par la structure](https://asknature.org/strategy/wing-scales-cause-light-to-diffract-and-interfere/)
* [Why Denim Fades – A Scientific Explanation](https://www.heddels.com/2017/05/why-denim-fades-a-scientific-explanation/)
* Wikipedia : [Light](https://en.wikipedia.org/wiki/Light#Electromagnetic_spectrum_and_visible_light), [Ultraviolet](https://en.wikipedia.org/wiki/Ultraviolet), [Morpho](https://fr.wikipedia.org/wiki/Morpho), [Indigo](https://en.wikipedia.org/wiki/Indigo), [Indigo dye](https://en.wikipedia.org/wiki/Indigo_dye), [Indigo carmine](https://en.wikipedia.org/wiki/Indigo_carmine), [Photodegradation](https://en.wikipedia.org/wiki/Photodegradation), [Lightfastness](https://en.wikipedia.org/wiki/Lightfastness), [Hydrolysis](https://en.wikipedia.org/wiki/Hydrolysis)

### Projet

* Wikipedia : [Computer-aided design](https://en.wikipedia.org/wiki/Computer-aided_design)
