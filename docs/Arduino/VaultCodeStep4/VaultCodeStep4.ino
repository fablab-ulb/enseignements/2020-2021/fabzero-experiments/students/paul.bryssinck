/*
 * File name : VaultCodeStep4
 * Ask the user to enter a code and flash a LED in return
 * Autor : Paul Bryssinck
 * Last modified : 2021-04-21
 * License : CC BY (https://creativecommons.org/licenses/by/4.0/)
*/


// on définit des variable (et notamment on renomme les pins ce qui est plus pratique que des numéros
int pinRed = 4;
int pinGreen = 3;
int pinBlue = 2;

int pinA = 9; // Connected to CLK 
int pinB = 8;  // Connected to DT
int button = 7; // Connected to SW (switch ?)
int encoderPosCount = 0; //la position virtuelle du potentiomètre

int pinALast; //dernière valeur du pin A
int pinAValue; // la valeur lue du pin A 
boolean bCW; //true si clockwise

int buttonLast;
int buttonValue;
int codeLastValue;

boolean codeFini; // pour arreter le programe une fois que le code correct est entré

void setup() { 
  codeFini = false;
  pinMode (pinA,INPUT);
  pinMode (pinB,INPUT);
  pinMode (button,INPUT);
  /* Read Pin A Whatever state it's in will reflect the last position   */
  pinALast = digitalRead(pinA);
  buttonLast = digitalRead(button);
  Serial.begin (9600); /*This starts serial communication, so that the Arduino can send out commands through the USB connection. 
  The value 9600 is called the 'baud rate' of the connection. This is how fast the data is to be sent. 
  You can change this to a higher value, but you will also have to change the Arduio Serial monitor to the same value. */
  Serial.println("73.09.11");
  digitalWrite(pinRed,LOW);
  digitalWrite(pinGreen,LOW);
  digitalWrite(pinBlue,LOW);
  }
  

void loop() {
  

  // as long as the code has not been entered properly
  while (codeFini==false){
    pinAValue = digitalRead(pinA);
    buttonValue = digitalRead(button);
  // if rotation
    if (pinAValue != pinALast){ // Means the knob is rotating
      // if the knob is rotating, we need to determine direction
      // We do that by reading pin B.
      if (digitalRead(pinB) != pinAValue) {  // Means pinB has not yet changed, so we'rerotating clockwise
        encoderPosCount ++;
        bCW = true;
        } 
      else {// Otherwise B changed first and we're moving CCW
        bCW = false;
        encoderPosCount--;
        }
      Serial.print("Encoder Position: ");
      Serial.println(encoderPosCount);
      pinALast = pinAValue;
      } 
    
  //if button
    if (buttonValue != buttonLast and codeFini==false){
      buttonLast=buttonValue;
      if (buttonValue == 0){ // si on vient d'appuyer sur le bouton
        if (encoderPosCount == 73){
          Serial.print(encoderPosCount);
          Serial.println(", validé."); 
          codeLastValue = 73;
          // for a correct value i make the RGBLed flash green
          flash();
          }
        else if (encoderPosCount==9 and codeLastValue == 73){
          Serial.print(encoderPosCount);
          Serial.println(", validé."); 
          codeLastValue = 9;
          flash();
          }
        else if (encoderPosCount==11 and codeLastValue == 9){
          Serial.print(encoderPosCount);
          Serial.println(", validé.");
          Serial.println(" Fin du code.");  
          codeLastValue = 11;
          codeFini=true;
          flash();
          }
        else{
          Serial.print("erreur. entrez les nombres affichés.");
          Serial.println("73.09.11");
          codeLastValue = 0;
          flashRed();
          }
        }
    }
   }

   // Once the code is correctly given
  digitalWrite(pinGreen,HIGH);
  digitalWrite(pinRed,LOW);
  digitalWrite(pinBlue,LOW);
  delay(250);
  digitalWrite(pinGreen,LOW);
  digitalWrite(pinRed,HIGH);
  digitalWrite(pinBlue,LOW);
  delay(250);
  digitalWrite(pinGreen,LOW);
  digitalWrite(pinRed,LOW);
  digitalWrite(pinBlue,HIGH);
  delay(250);
}

void flashRed(){
  digitalWrite(pinRed, HIGH);
  delay(1000);
  digitalWrite(pinRed, LOW);
}

void flash(){
  digitalWrite(pinGreen,HIGH);
  delay(100);
  digitalWrite(pinGreen,LOW);
  delay(100);
  digitalWrite(pinGreen,HIGH);
  delay(100);
  digitalWrite(pinGreen,LOW);
  delay(100);
  digitalWrite(pinGreen,HIGH);
  delay(100);
  digitalWrite(pinGreen,LOW);
  delay(100);
  digitalWrite(pinGreen,HIGH);
  delay(100);
  digitalWrite(pinGreen,LOW);
  delay(100);
  digitalWrite(pinGreen,HIGH);
  delay(100);
  digitalWrite(pinGreen,LOW);
  delay(100);
  digitalWrite(pinGreen,HIGH);
  delay(100);
  digitalWrite(pinGreen,LOW);
  delay(100);
  digitalWrite(pinGreen,HIGH);
  delay(100);
  digitalWrite(pinGreen,LOW);
  delay(100);
  digitalWrite(pinGreen,HIGH);
  delay(100);
  digitalWrite(pinGreen,LOW);
}
