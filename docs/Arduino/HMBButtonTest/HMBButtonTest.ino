/*
 *  AUTEUR : Nicolas De Coster
 *  
 *  Reproduis par : Paul Bryssinck
 *  le : 29-04-2021
 */


#define LED 15
#define BUTTON 8
#define DEBOUNCE 10

void setup() {
  pinMode(15,OUTPUT);
  pinMode(8,INPUT_PULLUP);
  digitalWrite(LED,LOW);
  SerialUSB.begin(0);
}

void loop() {
  // if button is pressed (sinal is low)
  if (!digitalRead(BUTTON)){
    digitalWrite(LED,!digitalRead(LED));
    SerialUSB.println("can't touch this");
    // debounce button
    delay(DEBOUNCE);
    //traped in this loop until release of button
    while(!digitalRead(BUTTON)){}
    delay(DEBOUNCE);
  }
}
