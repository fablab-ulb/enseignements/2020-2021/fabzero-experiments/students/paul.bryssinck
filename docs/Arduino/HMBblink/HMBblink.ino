/*
 *  AUTEUR : Nicolas De Coster
 *  
 *  Reproduis par : Paul Bryssinck
 *  le : 29-04-2021
 */

#define LED 15
#define INTERVAL 1000

void setup() {
  pinMode(15,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(LED, HIGH);
  delay(INTERVAL/2);
  digitalWrite(LED, LOW);
  delay(INTERVAL/2);
}
