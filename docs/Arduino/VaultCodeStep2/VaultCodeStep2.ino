/*
 * File name : VaultCodeStep2
 * Keep track of the rotation and pressing of the module
 * Autor : Paul Bryssinck
 * Last modified : 2021-04-21
 * License : CC BY (https://creativecommons.org/licenses/by/4.0/)
*/

// on définit des variable (et notamment on renomme les pins ce qui est plus pratique que des numéros
int pinA = 9; // Connected to CLK 
int pinB = 8;  // Connected to DT
int button = 7; // Connected to SW (switch ?)
int encoderPosCount = 0; //la position virtuelle du potentiomètre

int pinALast; //dernière valeur du pin A
int pinAValue; // la valeur lue du pin A 
boolean bCW; //true si clockwise
boolean released;

int buttonLast;
int buttonValue;
// pas besoin de : boolean released ; // switch to true when button change from 0 (=LOW, pressed) to 1 (=HIGH, released) and to false from 1 to 0

void setup() { 
  pinMode (pinA,INPUT);
  pinMode (pinB,INPUT);
  pinMode (button,INPUT);
  /* Read Pin A Whatever state it's in will reflect the last position   */
  pinALast = digitalRead(pinA);
  buttonLast = digitalRead(button);

  released = true;
  Serial.begin (9600); /*This starts serial communication, so that the Arduino can send out commands through the USB connection. 
  The value 9600 is called the 'baud rate' of the connection. This is how fast the data is to be sent. 
  You can change this to a higher value, but you will also have to change the Arduio Serial monitor to the same value. */
  } 

void loop() { 
  pinAValue = digitalRead(pinA);
  buttonValue = digitalRead(button);

  //if button
  if (buttonValue != buttonLast){
    buttonLast=buttonValue;
    if (buttonValue == 0){
      Serial.print("position ");
      Serial.print(encoderPosCount);
      Serial.println(" validée.");
    }
  }
  // if rotation
  if (pinAValue != pinALast){ // Means the knob is rotating
    // if the knob is rotating, we need to determine direction
    // We do that by reading pin B.
    if (digitalRead(pinB) != pinAValue) {  // Means pinB has not yet changed, so we'rerotating clockwise
      encoderPosCount ++;
      bCW = true;
      } else {// Otherwise B changed first and we're moving CCW
        bCW = false;
        encoderPosCount--;
        }
        Serial.print("Encoder Position: ");
        Serial.println(encoderPosCount);
        } 
        pinALast = pinAValue;
  }
