/*
 * File name : VaultCodeStep1
 * From the docs https://www.velleman.eu/products/view/?id=439226
*/
// on définit des variable (et notamment on renomme les pins ce qui est plus pratique que des numéros
int pinA = 9; // Connected to CLK 
int pinB = 8;  // Connected to DT
int encoderPosCount = 0; //la position virtuelle du potentiomètre
int pinALast; //dernière valeur du pin A
int aVal; // la valeur lue du pin A 
boolean bCW; //true si clockwise

void setup() { 
  pinMode (pinA,INPUT);
  pinMode (pinB,INPUT);
  /* Read Pin A Whatever state it's in will reflect the last position   */
  pinALast = digitalRead(pinA);   
  Serial.begin (9600); /*This starts serial communication, so that the Arduino can send out commands through the USB connection. 
  The value 9600 is called the 'baud rate' of the connection. This is how fast the data is to be sent. 
  You can change this to a higher value, but you will also have to change the Arduio Serial monitor to the same value. */
  } 

void loop() { 
  aVal = digitalRead(pinA);
  if (aVal != pinALast){ // Means the knob is rotating
    // if the knob is rotating, we need to determine direction
    // We do that by reading pin B.
    if (digitalRead(pinB) != aVal) {  // Means pinB has not yet changed, so we'rerotating clockwise
      encoderPosCount ++;
      bCW = true;
      } else {// Otherwise B changed first and we're moving CCW
        bCW = false;
        encoderPosCount--;
        }
        Serial.print ("Rotated: ");
        if (bCW){
          Serial.println ("clockwise");
          }else{
            Serial.println("counterclockwise");
            }
            Serial.print("Encoder Position: ");
            Serial.println(encoderPosCount);
            } 
            pinALast = aVal;
            }
