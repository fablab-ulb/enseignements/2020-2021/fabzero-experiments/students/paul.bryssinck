/*
 * File name : VaultCodeStep3
 * Ask the user to enter a code with the button
 * Autor : Paul Bryssinck
 * Last modified : 2021-04-21
 * License : CC BY (https://creativecommons.org/licenses/by/4.0/)
*/

// on définit des variable (et notamment on renomme les pins ce qui est plus pratique que des numéros
int pinA = 9; // Connected to CLK 
int pinB = 8;  // Connected to DT
int button = 7; // Connected to SW (switch ?)
int encoderPosCount = 0; //la position virtuelle du potentiomètre

int pinALast; //dernière valeur du pin A
int pinAValue; // la valeur lue du pin A 
boolean bCW; //true si clockwise

int buttonLast;
int buttonValue;
int codeLastValue;

boolean codeFini = false; // pour arreter le programe une fois que le code correct est entré

void setup() { 
  pinMode (pinA,INPUT);
  pinMode (pinB,INPUT);
  pinMode (button,INPUT);
  buttonLast = digitalRead(button);
  /* Read Pin A Whatever state it's in will reflect the last position   */
  pinALast = digitalRead(pinA);
  Serial.begin (9600); /*This starts serial communication, so that the Arduino can send out commands through the USB connection. 
  The value 9600 is called the 'baud rate' of the connection. This is how fast the data is to be sent. 
  You can change this to a higher value, but you will also have to change the Arduio Serial monitor to the same value. */
  Serial.println("73.09.11");
  }
  

void loop() { 
  pinAValue = digitalRead(pinA);
  buttonValue = digitalRead(button);

  // if rotation
  if (pinAValue != pinALast){ // Means the knob is rotating
    // if the knob is rotating, we need to determine direction
    // We do that by reading pin B.
    if (digitalRead(pinB) != pinAValue) {  // Means pinB has not yet changed, so we'rerotating clockwise
      encoderPosCount ++;
      bCW = true;
      } 
    else {// Otherwise B changed first and we're moving CCW
      bCW = false;
      encoderPosCount--;
      }
    Serial.print("Encoder Position: ");
    Serial.println(encoderPosCount);
    pinALast = pinAValue;
    } 
    
  //if button
  if (buttonValue != buttonLast and codeFini==false){
    buttonLast=buttonValue;
    if (buttonValue == 0){ // si on vient d'appuyer sur le bouton
      if (encoderPosCount == 73){
        Serial.print(encoderPosCount);
        Serial.println(", validé."); 
        codeLastValue = 73;
        }
      else if (encoderPosCount==9 and codeLastValue == 73){
        Serial.print(encoderPosCount);
        Serial.println(", validé."); 
        codeLastValue = 9;
        }
      else if (encoderPosCount==11 and codeLastValue == 9){
        Serial.print(encoderPosCount);
        Serial.print(", validé.");
        Serial.println(" Fin du code.");  
        codeLastValue = 11;
        }
      else{
        Serial.print("erreur. entrez les nombres affichés.");
        Serial.println("73.09.11");
        codeLastValue = 0;
        }
      }
    buttonLast = buttonValue;
    }

  if (codeFini==true){
    
    }
  }

  
