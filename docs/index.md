# Page d'accueil #

Bienvenue sur mon site FabZero Experiments, que je complèterai durant ce premier quadrimestre 2021 pour rendre compte de mon apprentissage en fabrication numérique au [FabLab ULB](http://fablab-ulb.be/).

## Mais le site de qui ?

Le site de Paul, j'étudie la physique à l'ULB, une formation très orientée vers la théorie que j'essaie de diversifier un peu à l'aide de cours de biologie ou même à travers ce cours FabZero. J'arrive au bout de mon parcours étudiant et je travaille pour mon mémoire sur la capture de nectar par les abeilles.

Je ne sais pas si vous connaissez [Tadao Ando](https://fr.wikipedia.org/wiki/Tadao_And%C5%8D), c'est un architecte japonais travaillant autour de formes géométriques épurées, souvent en béton, et avec une importance particulière accordée à la lumière. Il est notamment l'architecte du musée Chikatsu Asuka au Japon, que je trouve vraiment beau.

![Musée Chikatsu Asuka, Japon](images/museum.jpg "Musée Chikatsu Asuka, Japon")

#### Et quelques infos comme ça pour remplir la page :

* Je suis en train de relire _Dune_, de Frank Herbert, j'accroche vraiment au style d'écriture, j'ai hâte de voir sa nouvelle adaptation cinéma
* La 5e saison de The Expanse est sortie, je n'ai plus beaucoup d'espoir mais sait-on jamais ça pourrait redevenir bien (edit : pas mal.)
* Ne louez pas une chambre avec du carrelage, en hiver c'est froid
* Mon oxalis va bien

![Oxalis Pourpre](images/oxalis.jpg "L'oxalis pourpre de Paul survit aux attaques répétées des chats de la maison")
