/*
    FILE    : FFBeam-OutOfPlane.scad
    
    AUTHOR  : Paul Bryssinck <paulbryssinck@hotmail.com>
    
    DATE    : 2021-02-27
    
    LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0) https://creativecommons.org/licenses/by/4.0/
    
    Original design by the BYU Compliant Mechanisms Research Group (CMR) (https://www.compliantmechanisms.byu.edu/flexlinks)
*/

//parametres
$fn=100;
pi=3.1415926535;
thick=3; //epaisseur des fixations (direction y)
//fixation :
nHoles=2; //nombre de trous par fixation (détermine aussi la taille de la fixation
rOut=4; //largeur fixation (direction z) (la longueur de la fixation est nHoles*2*rOut)
rInn=2.5; //taille des trous
holeSep=0;//pour controler l'espacement entre les trous
//liaison :
linkLen=20; //longueur de la tige depliee
linkWid=1.5; //epaisseur de la liaison
r=2*linkLen/pi; //on calcule les rayon exterieur et interieur des cylindres qu'on va utiliser pour décrire la liaison
linkOutRad=r+linkWid/2; 
linkInnRad=r-linkWid/2;

//coeur de fixation :
module filledClip(xPos,yPos,zPos){
    hull(){
        translate([0,0,thick/2])cube([2*rOut,2*rOut,thick], center=true);
        translate([xPos,yPos,zPos])cylinder(h=thick, r=rOut);
        }
}

//liaison :
module link(xPos,yPos,zPos){
    //on prend la différence entre des cylindres de rayon rOut et rInn
    //ça nous donne un cercle complet, on retirera la moitié plus tard
    difference(){
        translate([xPos,yPos,zPos])cylinder(h=2*rOut, r=linkOutRad);
        translate([xPos,yPos,zPos])cylinder(h=2*rOut, r=linkInnRad);
        }
}

//Assemblage et perçage des différentes parties
module assembly(){
    
    union(){
        //fixation 1 (touchant l'axe y<0)
        translate([-rOut,-r-thick/2,rOut])rotate([90,0,180])
        difference(){
            filledClip(2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0);
            for(i=[1:nHoles]){
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick,r=rInn);
            }
        }
        //fixation 2 (touchant l'axe y>0)
        translate([-rOut,r-thick/2,rOut])rotate([90,0,180])
        difference(){
            filledClip(2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0);
            for(i=[1:nHoles]){
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick,r=rInn);
            }
        }
        
        //on cree la liaison : un anneau dont on retire la moitié :
        difference(){ 
            link(0,0,0);
            translate([-linkOutRad/2,0,rOut])cube([linkOutRad,2*linkOutRad,2*rOut],center=true);
        }
        }
}
assembly();

        
