/*
    FILE    : cantileverBeam.scad

    AUTHOR  : Paul Bryssinck <paulbryssinck@hotmail.com>

    DATE    : 2021-05-05

    LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0) https://creativecommons.org/licenses/by/4.0/

    Original design by the BYU Compliant Mechanisms Research Group (CMR) (https://www.compliantmechanisms.byu.edu/flexlinks)
*/

//parametres
$fn=100; //il est important d'avoir un nombre de faces assez élevé pour que les trous soient lisses
         //(sinon ils ne se clipseront pas sur les Lego).
nHoles=3; //nombre de trous par fixation (détermine aussi la taille de la fixation)
thick=3; //l'epaisseur de la fixation (direction z)
rOut=4;  //la largeur de la fixation (direction y)
         //la longueur de la fixation (direction x) est nHoles*2*rOut
rInn=2.5; //le rayon des trous
linkLen=70; //la longueur de la tige (direction x)
linkWid=1.5; //l'epaisseur de la tige (direction y)
             //la largeur de la tige (direction z) vaut rOut
holeSep=0; //controle l'espacement entre les trous, pas necessaire pour le standard Lego


/*
  coeur de la fixation :
  
    prend l'enveloppe (fonction "hull") de deux cylindre, de rayon rOut, que j'espace (fonction "translate") en fonction du nombre de trous qu'on fera dedans.
*/
module filledClip(){
    hull(){cylinder(h=thick, r=rOut);
translate([2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0])cylinder(h=thick, r=rOut);}
}

/*
  cantilever (la tige) :

    la tige est formée de l'enveloppe de deux cylindres, qu'on tourne et déplace de façon à avoir une extremité dans la fixation (pour être precis, le bout de la tige arrive pile au centre du trou le plus à droite de la fixation (comme on creuse le trou apres avoir placé la tige c'est pas un probleme)).
*/
module cantilver(){
    dist=(nHoles-1)*(2*rOut+holeSep); //la position x du début début de la tige (dans la fixation)
hull(){
translate([dist,linkWid/2,thick/2])rotate([90,0,0])cylinder(r=thick/2,h=linkWid);
translate([dist+linkLen,linkWid/2,thick/2])rotate([90,0,0])cylinder(r=thick/2,h=linkWid);}
}

/*
  Assemblage et perçage de la pièce :

    la fonction difference prend son premier argument (ici l'union de la fixation et de la tige) et en retire le second argument (ici les trous, défini comme des cylindres dans la boucle for).
*/
module assembly(){
    difference(){union(){cantilver();filledClip();}
    for(i=[1:nHoles]){
        translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick,r=rInn);}
    }
}

//On cree la piece, qu'on déplace pour que le début de la tige soit centree en l'origine 
//(c'est juste visuel, on pourrait simplement appeller "assembly();").
translate([-(nHoles)*(2*rOut+holeSep)+rOut+holeSep,0,0])assembly();