/*
    FILE    : FSBeam-straight.scad
    
    AUTHOR  : Paul Bryssinck <paulbryssinck@hotmail.com>
    
    DATE    : 2021-02-27
    
    LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0) https://creativecommons.org/licenses/by/4.0/
    
    Original design by the BYU Compliant Mechanisms Research Group (CMR) (https://www.compliantmechanisms.byu.edu/flexlinks)
*/

//parametres
$fn=100;
//fixation:
nHoles=2;
rOut=4; //largeur fixation (direction y) (la longueur de la fixation est nHoles*2*rOut)
rInn=2.5; //taille des trous
thick=3; //epaisseur fixation (direction z)
holeSep=0;//pour controler l'espacement entre les trous
//tige
linkLen=70; //longueur de la tige
linkWid=1.5; //epaisseur tige (direction y)
//bout libre
slotLen=6*rOut; //taille de l'extremité libre

//coeur de la fixation :
module filledClip(xPos,yPos,zPos){
    hull(){
        cylinder(h=thick, r=rOut);
        translate([xPos,yPos,zPos])cylinder(h=thick, r=rOut);
        }
}

//extremité libre (pleine)
module filledSlot(xPos,yPos,zPos){
    hull(){
        translate([xPos,yPos,zPos])cylinder(h=thick, r=rOut);
        translate([xPos+slotLen,yPos,zPos])cylinder(h=thick, r=rOut);}
}

//cantilever (la tige) :
module link(){
    dist=(nHoles-1)*(2*rOut+holeSep); //le début de la tige : dans la fixation
hull(){
    translate([dist,linkWid/2,thick/2])rotate([90,0,0])cylinder(r=thick/2,h=linkWid);
    translate([dist+linkLen,linkWid/2,thick/2])rotate([90,0,0])cylinder(r=thick/2,h=linkWid);}
}

//Assemblage et perçage des différentes parties
module assembly(){
    difference(){
        difference(){
            union(){
                link();
                filledClip(2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0);
                filledSlot(2*rOut*(nHoles-1)+holeSep*(nHoles-1)+linkLen,0,0);
                }
            for(i=[1:nHoles]){
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick,r=rInn);
                }
            }
        hull(){
            translate([(2*rOut+holeSep)*(nHoles-1)+linkLen,0,0])cylinder(h=thick, r=rInn);
            translate([(2*rOut+holeSep)*(nHoles-1)+linkLen+slotLen,0,0])cylinder(h=thick, r=rInn);}
        }
}

assembly();

        
