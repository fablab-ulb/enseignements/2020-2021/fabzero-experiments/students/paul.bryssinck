# 5. Electronique I - Prototypage

<div class="jumbotron">
  <h4>Objectifs</h4>
  <p class="lead">
  <ul>
    <li>Make a basic exercice using a development board <a href="https://www.arduino.cc/en/Tutorial/BuiltInExamples">examples here</a></li>
    <li>Measure something: add a sensor to a development board and read it.</li>
    <li>Make your board do something : add an output device to a development board and program it to do something
</li>
  </ul>
  </p>
</div>

Pour ce premier module sur l'électronique, nous portons notre attention sur l'environnement de développement électronique [Arduino](https://en.wikipedia.org/wiki/Arduino). Par cette appellation, on entend aussi bien la partie hardware, principalement les carte type Arduino Uno ainsi que les périphériques qu'on peut y connecter, que la partie software, c'est à dire l'IDE Arduino et les librairies qu'on peut trouver sur internet.

## L'Arduino Uno
La pièce centrale d'une carte Arduino Uno est son microcontrôleur, un Atmega328p, le gros rectangle gris sur le plan. Un microcontrôleur est une puce électronique combinant les différentes parties élémentaires d'un ordinateur traditionnel, c'est-à-dire un processeur pour effectuer des opérations logiques, de la mémoire pour stocker et utiliser les informations sur lesquelles effectuer ces opérations, ainsi que des canaux de communication avec l'extérieur de la puce pour pouvoir recevoir et transmettre des données.

Autour de cette unité centrale, on trouve notamment des pin d'input/output numériques et des pins d'input analogiques, qui permettent de brancher toutes sortes de choses à la carte, de la simple LED au détecteur de température, en passant par le capteur de mouvement ou le codeur rotatif.

Certains pins numériques sont labelisés PWM (_Pulse-Width Modulation_). Ces pins permettent de simuler un output analogique, en ouvrant et fermant la sortie (fixée à 5 Volts) à une fréquence choisie de façon à ce qu'en moyenne l'intensité mesurée soit celle désirée. Nos yeux font naturellement cette "moyenne", si on fait clignoter une LED à une fréquence suffisamment élevée, nous percevront une intensité constante mais plus faible que celle correspondant aux 5 volts. En pratique, un circuit RC permet d'effectuer cette moyenne.

![Arduino Uno](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/raw/master/img/Arduino-uno-pinout.png)

En combinant ainsi n'importe quelle pièce externe à la carte et son microcontrôleur, on peut imaginer une myriade d'application. Un exemple ? Vous supportez mal la chaleur en été et souhaitez optimiser la température de votre appartement ? Rien de plus simple, branchez votre capteur de température, un haut-parleur, et demandez à votre carte Arduino de sonner l'alarme lorsque la température extérieure dépasse un certain seuil pour vous rappeler de fermer les fenêtres.
""**Demander à la carte**" ? Oui, c'est l'aspect software mentionné plus haut, c'est bien beau de brancher tout ce matériel ensemble, encore faut-il expliquer à la carte ce qu'elle est sensée en faire, on y reviendra après l'aspect hardware. Toujours est-il que pour transmettre ces instructions, il faut pouvoir connecter la carte à un ordinateur. La façon la plus "User-friendly" d'y parvenir est via le port USB, qui permet également d'alimenter la carte (alternativement à la prise jack). Il est également possible de programmer le microcontrôleur via les pins labelisés ICSP sur la carte (pour _In-Circuit Serial Programming_), une approche différente de la communication par USB qui permet d'envoyer des instructions à la carte pendant son utilisations (mais dont les tenants et aboutissants m'échappent, [la page Wikipédia](https://en.wikipedia.org/wiki/In-system_programming) pourra éclairer d'avantage le lecteur curieux).

Il y a encore d'autres éléments intégrés sur la carte, notamment des LED ou une puce faisant interface entre le port USB et le microcontrôleur qui nativement ne gère pas le protocole USB.

#### Précautions d'emploi

* L'Arduino Uno fonctionne en 5 Volts, il est donc impératif de ne pas brancher d'appareils avec une plus grande ddp dessus (la plupart de nos appareils ménager fonctionnent en 12 Volts)

* Chaque pin peut supporter jusqu'à 40 mA de courant, mais il est recommandé de ne pas dépasser 20 mA.

* Les pin sont regroupés en groupes identifiables sur la carte par un code couleur. Le courant total dans chaque groupe ne doit pas dépasser 100 mA

* Le courant maximal supporté par l'ensemble de l'Arduino Uno est de 200 mA.

## L'IDE Arduino

Pour programmer les cartes, on passera par l'[IDE Arduino](https://www.arduino.cc/en/software/) :
[IDE Arduino](../images/5.Electronique/IDEArduino.jpg)
C'est ici qu'on écrira les instructions à charger sur la carte.

Deux méthodes sont présentes par défaut, setup et loop.
La première s'exécute au lancement du programme, tandis que la seconde est celle qui tournera en boucle après.

## Premier test - Une lumière dans la nuit

Commençons par un petit projet pour se familiariser avec le matériel, et faisons clignoter une led.
Le schéma fourni dans le kit d'initiation pour Arduino Uno permet de facilement reproduire le circuit

![projet 2 - flashing LED schematics](../images/5.Electronique/LED.jpg)

Il ne reste plus qu'à écrire le programme, ici c'est facile, un des programmes par défaut, "blink", fait clignoter la LED intégrée à la carte. Il suffit alors de changer quelques paramètres pour faire clignoter la LED branchée sur la platine de prototypage (la _breadboard_ quoi).

<video controls>
<source src="../../images/5.Electronique/SOSMuted.mp4" type="video/mp4">
Premier test Arduino Uno : faire clignoter une LED
</video>


## RGB LED

Ce projet ne m'a pas appris grand-chose de plus, mais j'étais vraiment curieux de voir une LED RGB en action.
(Ça m'a tout de même permis de me rendre compte que sur le schéma, les pattes Green et Blue sont inversées, "Faites confiance mais vérifiez".)
![projet 2 - flashing LED](../images/5.Electronique/RGBLED.jpg)

## Traitement d'Input - Capteur rotatif

Pour apprendre à traiter des informations de l'environnement, j’ai choisi un codeur rotatif ([Digital rotary encoder module](https://www.velleman.eu/products/view/?id=439226)) qui permet de mesurer la rotation d'un bouton et son activation. Lors de la rotation du bouton, deux signaux sont envoyés légèrement en décalage, ce qui permet de déterminer le sens de rotation (selon lequel des deux est détecté en premier).

<div class="card">
  <div class="card-body">
    <p class="text-muted">
    <b>Remarque : </b> C'est avec  ce type de pièce qu'on contrôle les imprimantes 3D Prusa que nous utilisons au FabLab.
    </p>
  </div>
</div>

Pour apprendre à utiliser cette pièce, j'ai pensé à un petit montage, dont le but est de faire entrer un code par l'utilisateur, que je décompose en plusieurs étapes :

1. Copier un petit code fourni en exemple dans la documentation et le décortiquer, comprendre ce que fait chaque ligne.
2. Ajouter l'utilisation du bouton pressoir (l'exemple n'utilise que la rotation du bouton)
3. Ajouter un code secret que l'utilisateur doit entrer avec l'aide du bouton (sélection du nombre avec la rotation et validation avec la pression)
4. Ajouter une réponse visuelle avec la LED RGB

##### 1. Premier test
Voici le montage et le code fourni dans la [documentation](https://www.velleman.eu/downloads/29/vma435_a4v01.pdf) du capteur rotatif (j'ai ajouté quelques commentaires dans le code qui explicitent certaines lignes) :

![circuit de base pour DREM](../images/5.Electronique/DREMcircuit.jpg)

```
// Définition des variables
int pinA = 9; // Connected to CLK
int pinB = 8;  // Connected to DT
int encoderPosCount = 0; //la  variable associée à la position angulaire du bouton
int pinALast; //dernière valeur du pin A
int aVal; // la valeur lue du pin A
boolean bCW; //true si clockwise

void setup() {
  pinMode (pinA,INPUT);
  pinMode (pinB,INPUT);
  /* Read Pin A Whatever state it's in will reflect the last position   */
  pinALast = digitalRead(pinA);   
  Serial.begin (9600); /*This starts serial communication, so that the Arduino can send out commands through the USB connexion.
  The value 9600 is called the 'baud rate' of the connection. This is how fast the data is to be sent.
  You can change this to a higher value, but you will also have to change the Arduino Serial monitor to the same value. */
  }

void loop() {
  aVal = digitalRead(pinA);
  if (aVal != pinALast){ // Means the knob is rotating
    // if the knob is rotating, we need to determine direction
    // We do that by reading pin B.
    if (digitalRead(pinB) != aVal) {  // Means pinB has not yet changed, so we're rotating clockwise
      encoderPosCount ++;
      bCW = true;
      } else {// Otherwise B changed first and we're moving CCW
        bCW = false;
        encoderPosCount--;
        }
        Serial.print ("Rotated: ");
        if (bCW){
          Serial.println ("clockwise");
          }else{
            Serial.println("counterclockwise");
            }
            Serial.print("Encoder Position: ");
            Serial.println(encoderPosCount);
            }
            pinALast = aVal;
            }
```

Ce code détecte le sens de rotation du bouton et l'affiche, ainsi qu'une position calculée par rapport à la position initiale 0, dans le moniteur série.

![](../images/5.Electronique/moniteurSerie.jpg)

Ce premier test m'a permis de me rendre compte d'une chose. Le bouton n'est pas complètement libre dans sa rotation, on peut le tourner par pas de un vingtième de tour. Mais lors de la rotation d'un pas, l'Arduino détecte deux changements de positions, ce qui peut se lire sur le moniteur série, la position varie toujours de +/- 2.

##### Autres étapes

J'ai d'abord écrit les codes Arduino pour les autres étapes, hors du FabLab et donc sans le matériel électronique. Bien que les code compilaient, lorsque je les ai testés ça ne marchait pas comme je voulais, mais heureusement je n'ai du faire que des modifications mineurs (tous les codes (commençant par "_VaultCode_") sont disponibles dans mon repository GitLab, dans le dossier [Arduino](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/tree/master/docs/Arduino)).

Le code le plus élaboré fonctionne comme suit :
* le programme commence par afficher le code "73 09 11" dans le moniteur série.
* en tournant le bouton, l'utilisateur peut sélectionner un nombre et le valider en appuyant sur le bouton
  * si le nombre ne correspond pas au code, la led s'allume en rouge et le code réapparais dans le moniteur.
  * si c'est le bon nombre, la led clignote en vert et l'utilisateur peut continuer
* si les trois nombres corrects sont entrés successivement, la led passe par ses trois couleurs en boucle.

Voici une vidéo qui montre en parallèle un utilisateur et le moniteur série.
<video controls>
<source src="../../images/5.Electronique/cc.mp4" type="video/mp4">
arduino input output
</video>
