# 4. Découpe assistée par ordinateur

<div class="jumbotron"> <!-- code adapté de l'exemple fourni par bootswatch pour le thème darkly -->
  <h4>Objectifs</h4>
  <p class="lead">
  <ul>
    <li>characterize your lasercutter's focus, power, speed, kerf, ... for folding and cutting cardboard paper</li>
    <li>design, lasercut, and document a kirigami (cut and folds) to make a 3D object</li>
    <li>cut something on the vinyl cutter</li>
  </ul>
  </p>
</div>

## Présentation des machines
 Axel nous a présenté plusieurs machines de découpe : deux découpeuses laser et une découpeuse vinyle.

 Les informations relatives aux découpeuses laser peuvent être consultées [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/laserCutters.md), nous allons utiliser l'Epilog et la Muse, la lasersaur étant en maintenance.

## Calibration

Avant de passer au design et a la découpe de nos pièces, il est important de bien connaitre son matériau et l'effet du laser dessus, en fonction de l'intensité et de la vitesse de passage de ce dernier. Pour cela, il est courant de réaliser des plaques de calibration
![3 plaques de calibration pour découpeuse laser : sur bois, carton et papier](../images/4.Decoupeuse/calibrationSheet.jpg "plaques de calibration")

Ces deux premières plaques ont été réalisées avec la découpeuse Epilog, nous avons fait la 3e sur la Muse pour tester le papier que nous allons utiliser

## Sticker et découpeuse Vinyle

Pour me familiariser avec la découpeuse vinyle je me fixe comme objectif de faire un auto-collant flocon de neige.
Pour les découpeuses il faut des fichiers en dessin vectoriel, qu'on peut réaliser sur inkscape, mais comme je ne maitrise pas du tout le logiciel je suis passé par l'intermédiaire de FreeCAD. Avec l'outil sketch, j'ai pu contraindre la géométrie du flocon pour obtenir les proportions voulues.
![image FreeCAD](../images/4.Decoupeuse/snowflake1.jpg)

 Ce sketch je peux l'exporter en .svg pour l'ouvrir sur inkscape. A ce moment le flocon est en trait, si je le passe dans la découpeuse vinyle elle va juste tracer des lignes, je ne pourrai rien détacher de la feuille.

 ![image FreeCAD](../images/4.Decoupeuse/snowflakeInkscapeTrait.jpg)

Il est possible d'épaissir le trait puis de faire basculer le trait en un contour.
![image FreeCAD](../images/4.Decoupeuse/snowflakeInkscapeContour.jpg)

Finalement, j'exporte ce fichier au format .dxf pour pouvoir l'ouvrir dans le logiciel de la découpeuse vinyle, Silhouette Studio.
L'ajout d'un cadre (ici en rouge) permet de demander à la découpeuse vinyle de passer légèrement sur les traits en noir de façon à pouvoir détacher l'autocollant, et de découper complètement le cadre rouge pour séparer l'autocollant du rouleau de vinyle. On peut spécifier dans le programme quel type de matériaux correspond à chaque couleur, ici "vinyle mat" pour le noir et "Papier transfert d'image" pour le rouge, ce qui permet le réglage automatique de la profondeur, force et vitesse de la lame. En pratique cependant, à moins que la lame soit neuve, on peut augmenter un peu la force et la profondeur. De toutes façons, il est toujours prudent d'effectuer un test avant de lancer la découpe.

![capture d'écran flocon ouvert sur silhouette studio](../images/4.Decoupeuse/silhouette.jpg)

[](../images/4.Decoupeuse/silhouetteTest.jpg)

Enfin le résultat tant attendu :

![stickerGSM](../images/4.Decoupeuse/stickerGSM.jpg)

## Kirigami et découpeuse laser
Un des aspects fascinant des origamis et kirigamis est l'émergence de structures tri-dimensionnelles complexe à partir de simple feuilles.

<div class="card">
  <div class="card-body">
    <p class="text-muted">
    <b>Remarque : </b> Un <i>Origami</i> n'implique que des pliages, alors qu'un <i>Kirigami</i> peut également impliquer de la découpe.
    </p>
  </div>
</div>

J'ai reproduis un [origami du BYU-CMR](https://www.instructables.com/Origami-Ballistic-Barrier/), conçu pour pouvoir être utilisé comme une barrière déployable. En me basant sur le schéma fourni, j'ai dessiné et contraint un sketch sur FreeCAD,

![origami Sketch](../images/4.Decoupeuse/origamiShieldSketch.jpg)

Puis je l'ai exporté sur Inkscape, où j'ai pu séparer le contour à découper du reste du pattern qu'on souhaite juste entailler. Le résultat est contenu dans un fichier .svg

![origami SVG](../images/4.Decoupeuse/origamiSVG.jpg)

Ce fichier .svg s'ouvre directement dans le logiciel de la découpeuse laser, on spécifie la vitesse et la puissance du laser pour les deux couleurs (vitesse de 60%, puissance de 2% pour le bleu et de 4% pour le rouge), et on peut lancer la découpe ! (si on n'est pas certain du placement du support dans lequel on va découper ou du dessin dans le logiciel, il y a une option qui fait tracer le contour du dessin par la tete de découpe.)
voilà le résultat :

![origami découpé](../images/4.Decoupeuse/origamiLaserCut.jpg)

Lors de la découpe j'ai dû faire un choix. Comme certaines lignes doivent se plier dans un sens et d'autres dans le sens inverse, l’idéal aurait été de faire des entailles des deux côtés de la feuille. Mais cela aurait impliqué de devoir faire une moitié de découpe, retourner la feuille puis faire une seconde découpe. Le risque en faisant ça, c’est de mal aligner la feuille une fois retournée, et j'ai donc choisi de faire toutes les découpes du même côté. Comme on peut s'y attendre, ça a rendu le pliage difficile, et j'ai commencé par faire une décalcomanie sur un papier fin. Au final, après quelques efforts (et avoir déchiré une partie de l'origami découpé au laser) j'ai réussi à plier les deux :

![origami découpé](../images/4.Decoupeuse/origamiBoth.jpg)

<video height="720" controls>
<source src="../../images/4.Decoupeuse/origami.mp4" type="video/mp4">
origami in action
</video>

Comme toujours les fichiers sont disponible sur mon site, à [cette adresse](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/tree/master/docs/cutters).
