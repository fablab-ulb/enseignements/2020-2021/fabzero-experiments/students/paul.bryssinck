# Module 3 : Impression 3D

<div class="jumbotron">
  <h4>Objectifs</h4>
  <p class="lead">
  <ul>
    <li>Concevoir un kit flexlinks permettant d'assemble un mécanisme flexible, en utilisant les pièces conçues par </li>
    <li>Imprimer le kit FlexLinks</li>
    <li>Créer et publier sur son site une vidéo du mécanisme fonctionnel (<10Mo).</li>
  </ul>
  </p>

  <hr class="my-4">

  <h4>À faire</h4>
  <p class="lead">
  <ul>
    <li>Corriger le design du kit pour le rendre fonctionnel avec des Lego</li>
  </ul>
  </p>
</div>


## 1. De la modélisation à l'impression

Poursuivant le travail de la semaine passée, Hélène et Gwendoline nous ont montrés comment transformer un fichier .stl en un fichier utilisable par les imprimantes 3D. Les machines dont dispose le FabLab sont des Prusa I3MK3S, c'est donc le slicer Prusa qu'on utilisera. (_slicer_ : logiciel qui sépare le modèle de l'objet en "g-file", un fichier reprenant la décomposition en différentes couches qui seront imprimées successivement.)

Tout d'abord, pour pouvoir paramétrer l'impression, il est important d'activer le mode expert, qui débloque l'accès à plusieurs options.

![Prusa Slicer GUI](../images/3.Impression3D/slicer.jpg "Flexlinks FFBeam open in Prusa Slice")
En dessous du bouton "Expert" il faut spécifier le rayon de la tête d'impression (ici 0.2 mm), le type de plastique qu'on imprime ainsi que le modèle de l'imprimante.

L'impression de la pièce se fait couche par couche, en commençant par le bas. Cela signifie qu'on ne peut pas juste imprimer une pièce dont une partie serait suspendue dans le vide. Pour faire ça, il faut indiquer au slice d'ajouter des supports. Le slicer détecte automatiquement les parties nécessitant un support (les lignes bleues), mais dans le cas de la pièce ci-dessous, il remplit les trous dans la pièce, et même si les supports sont supposés être facile à enlever, en pratique ça crée des irrégularités. Pour éviter ça, on peut demander au slicer de ne mettre des supports que sur le plateau d'impression.
![supports param](../images/3.Impression3D/slicerSupports.jpg)
![supports](../images/3.Impression3D/slicedSupport.jpg)

Pour des objets plus grands, ou si on souhaite imprimer plusieurs pièces en même temps, une bordure peut être ajoutée pour maintenir la pièce en place durant l'impression. La jupe quant à elle délimite la zone qu'occupera la pièce sur le plateau :
![jupe et bordure param](../images/3.Impression3D/slicerJupeEtBordures.jpg)
![jupe et bordure param](../images/3.Impression3D/slicedJupeEtBordure.jpg)

Enfin, on peut paramétrer le remplissage de la pièce, en choisissant le motif et sa densité. En pratique on ne dépasse jamais 35% en densité, et souvent aller au-delà de 10-15% n'apporte aucun avantage en termes de résistance aux contraintes mécaniques.
![remplissage](../images/3.Impression3D/slicerRemplissage.jpg)

Une fois tous les réglages fait, on peut cliquer sur "découper maintenant" ce qui crée le gcode, le fichier qu'on donnera à l'imprimante 3D.

Comme pièce test à imprimer, j'en ai choisi une simple, qui tenait à plat sur le plateau (ne requérant donc pas de supports), la double touillette :

![](../images/3.Impression3D/touillette.jpg "double-touillette vs mono-touillette")

On apprend de ses erreurs, ça tombe bien il y en a deux ici.

* les trous sont mal espacés, et il n'est pas possible d'attacher les deux trous d'une extrémité sur un Lego.

* le lien est trop épais pour être flexible.

C'est ici que la force de la conception paramétrique apparait, il m'a suffi de changer quelques valeurs dans ma liste de paramètres pour corriger ces erreurs.

J'ai mis les gcode que j'ai crée sur mon GitLab ([ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/tree/master/docs/3DPrintG-Code)).

## 2. Conception d'un mécanisme flexible

L'objectif de cette semaine est de concevoir un mécanisme avec des lego et des Flexlinks, un exercice que j'ai trouvé difficile.
Une des difficultés que j'ai rencontrées est la représentation, sur papier ou mentale d'un objet tri-dimensionnelle, et encore plus d'un mécanisme, impliquant donc un mouvement.
Une autre difficulté était celle du manque de but. Sans objectifs précis, sans problème donné à résoudre, je trouve qu'il est beaucoup plus difficile d'être créatif.
Ceci a donné lieu à plusieurs idées avortées car trop complexes.
![brouillon du mécanisme flexible](../images/3.Impression3D/compliantMech.jpg "plusieurs idées explorées, la plus simple, la pince, est retenue;")

Je me suis finalement rappelé que pour commencer c'était bien de faire au plus simple, et j'ai repensé à une pince du [BYU-CMR](https://www.compliantmechanisms.byu.edu/about-compliant-mechanisms) :

![pince d'un seul tenant fonctionnant grâce à des liaisons flexibles](../images/3.Impression3D/forceps.jpg)

Pour m'aider dans la conception, j'ai pensé m'aider de FreeCAD pour représenter le "Pseudo-rigid Body Model", mais j'ai déjà eu du mal a reproduire la pince alors que je voyais comment elle fonctionnait, je ne suis pas sûr que ce soit un outil réellement adapté à la création ex nihilo d'un mécanisme. Ça m'a quand même aidé à mieux comprendre le fonctionnement de la vraie pince.
![modélisation de la pince en pseudo-rigid body model sur FreeCAD](../images/3.Impression3D/pseudo-rigidBodyModel.jpg)

Pour reproduire cette pince avec des FlexLinks, j'ai imprimé ces pièces :
![pièces du FlexKit](../images/3.Impression3D/flexKitPieces.jpg)
Assemblé (avec les moyens du bord, je n'ai pas de Lego chez moi) ça donne ça :
![baguetteLink](../images/3.Impression3D/baguetteLink.jpg)

<video controls>
<source src="../../images/3.Impression3D/baguetteLinkDemo.mp4" type="video/mp4">
Test de la pince FlexLink
</video>

Le résultat, moyennement convainquant, n'est pas seulement dû au fait que j'ai scotché les FlexLinks aux baguettes, ce qui fait que les différentes pièces sont mobiles les unes par rapport aux autres. Le problème principal vient du fait qu'à cause des angles que j'ai choisi pour les FlexLinks (3/4 de tour, 1/4 de tour et 1/2 tour), la pince est sous tensions même quand on ne l'actionne pas. On peut le voir sur l'image ci-dessous, quand je laisse une attache libre, elle forme un angle d'environ 45° avec la baguette.

![tensions](../images/3.Impression3D/tensions.jpg)

Les fichiers des pièces du kit se trouvent à [cette adresse](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/tree/master/docs/CADFiles), ce sont celles dont les noms commencent par "compliantMechanism"
