# Module 1 : Outils de gestion de projet et documentation

<!-- Début du panneau -->
<div class="jumbotron"> <!-- code adapté de l'exemple fourni par bootswatch pour le thème darkly -->
  <h4><u>Objectifs</u></h4>
  <p class="lead">
  <ul> <!-- commencer une liste -->
    <li>Réfléchir à des <b>problématiques</b> nous tenant à cœur (les consigner sur la page final projet)
    <ul> <!-- sous liste -->
      <li>Pourquoi ça me tient à cœur</li>
      <li>poser le problème (<b>juste</b> le problème)</li>
      <li>rassembler des données sur le problème</li>
      </ul>
    </li>
    <li>Apprendre à utiliser les outils software nécessaire pour le cours (<b>Git</b>, <b>GitLab</b>, <b>Markdown</b>, <b>Bash</b>, <b>mkdocs</b>, etc.)</li>
    <li>Etablir la documentation de cette apprentissage</li>
  </ul>
  </p>

  <hr class="my-4"> <!-- trace une ligne -->

  <h4><u>Aller plus loin</u></h4>
  <p class="lead">
  <ul>
    <li>mieux comprendre le fonctionnement de linux et des commandes de base</li>
    <li>ne plus devoir entrer le mot de passe ssh à chaque push</li>
  </ul>
  </p>
</div>
<!--Fin du panneau -->



Cette première semaine est dédiée à l'apprentissage des différents outils informatique qui nous permettrons de documenter et partager ce que nous apprendrons et ferons pendant le cours.

## 1 Linux et bash
Il est désormais possible d'émuler linux et son terminal de commande sur windows. N'ayant pas la possibilité de partitionner mon disque dur pour installer directement une version de linux, c'est l'option que je choisi. Je combine plusieurs sources pour y parvenir ([site 1](https://korben.info/installer-shell-bash-linux-windows-10.html) ; [site2](https://itsfoss.com/install-bash-on-windows/)).

Une fois ubuntu installé, [ce tuto](https://www.howtogeek.com/261449/how-to-install-linux-software-in-windows-10s-ubuntu-bash-shell/) m'apprend à installer les packages dont je pourrais avoir besoin dans le futur (ex : traitement d'images, conversion markdown vers html)

#### 1.1 [Quelques commandes](https://ubuntu.com/tutorials/command-line-for-beginners#4-creating-folders-and-files) pour un usage quotidien ####

###### Créer des dossiers et se déplacer entre eux :
```
$ pwd #Print Working Directory

$ ls #renvoie le contenu du working directory

$ cd <path> #déplace le shell dans le dossier indiqué
$ cd .. #remonte au dossier parent
$ cd #remonte au working directory par défaut
$ cd /<chemin>/<dossier>/<sous-dossier> #est un chemin absolu ("/" symbolise la racine du disque dur, le dossier sans parent)
$ cd ~/<chemin>/<dossier> #est un chemin partant du working directory par défaut (indiqué par le "~")

$ mkdir <nomDossier> #crée un dossier dans le working directory
$ rm -rf <nomDossier> #supprime le dossier et son contenu
$ mv <chemin/dossierCible> <nouveauChemin> #deplace un dossier vers une nouvelle adresse
```
###### Créer et lire des fichiers :

```
$ echo <"some text"> affiche ce même texte
$ echo "ce qu'on veut écrire" > output.txt #redirige l'output de la commande echo vers un fichier texte (le créant au besoin)  
/!\ la commande ">" écrase le contenu du fichier !
$ ls > output.txt #on peut rediriger d'autres commande que echo

$ chmod -R 700 <dossier> #modifie les permissions pour le <dossier> ainsi que tout son contenu (grace à l'option -R). 700 donnent toutes permission à l'utilisateur et rien aux autres.
$ stat -c %a <dossier> #affiche les permissions du <dossier> sous forme 'octal' #ss
```
<div class="card">
  <div class="card-body">
    <p class="text-muted">
    <b>Remarque : </b>Où sont stockés tous ces dossiers et fichiers créés en environnement linux sur windows ? Dans mon cas, le chemin d'accès est C:\Users\Paul Bryssinck\AppData\Local\Packages\CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc\LocalState\rootf
    </p>
  </div>
</div>

#### 1.2 Conflit de permissions entre windows et ubuntu ####
Les fichiers créés via le terminal ubuntu sont stockés dans un des sous-dossiers de l'environnement linux, mais ils peuvent aussi bien être ouvert via ce terminal que via windows directement. En particulier, j'édite ces fichiers avec Atom, via l'interface windows donc. Le problème, c'est qu'éditer un fichier via windows efface les permissions d'accès d'ubuntu, ce qui fait qu'à chaque fois que j'apporte une modification, je dois utiliser la commande _chmod_ pour rétablir l'accès.
Pour contourner ce problème, j'ai écrit deux petits scriptes qui me permettent en une ligne de commande de me donner les permissions d'accès et de faire soit des push sur le repository gitlab, soit de construire la version locale du site web pour pouvoir visualiser le résultat en temps réel. (Ecrire les scriptes a été plus compliqué que prévu, car à cause de problème de compatibilité windows-linux (_encore_) j'ai du écrire les scriptes lignes par lignes dans le terminal de commande.)

## 2 Git

<div class="card">
  <div class="card-body">
    <p class="text-muted">
    <b>Remarque : </b>J'ai d'abord installé git sur windows, mais je suis retombé sur le vieux dossier git de BA3, pas celui que je voulais utiliser avec ubuntu. En fait l'installation de la console ubuntu se comporte vraiment comme une émulation de linux (sans le GUI), c'est donc sur ce terminal que j'ai réinstallé git.
    </p>
  </div>
</div>

Pour apprendre à utiliser git j'ai consulté [la page FabAcademy de Nicolas De Coster](http://archive.fabacademy.org/2018/labs/fablabulb/students/nicolas-decoster/?page=assignment&assignment=01).

Pour établir une connexion sécurisée entre les fichiers locaux et gitlab, j'ai créé une clef ssh en suivant [ces étapes](https://docs.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent).
#### 2.1 Commandes Git

```
$ man git <commande> #donne la doc sur la commande
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com

$ git clone https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics.git #crée une copie locale du repository (ici avec lien https, en pratique j'ai utilisé le lien ssh, pas sûr de ce que ça implique en fond).

$ git status #donne des infos sur le dossier cloné et le repository source (sont-ils sync, etc.)

$ git pull copie les changements fait sur la version originale sur le clone

$ git add <fichierModifé.ext> # ajoute le fichier modifier à la liste des fichiers qui seront envoyés lors du prochain commit
$ git add -A #Ajoute tous les fichiers modifiés à l'index

$ git commit -m "<message du comit>" #prepare les données à envoyer au repository cloné avec le message indiqué.

$ git push # envoie le commit au repository cloné
```
<div class="card">
  <div class="card-body">
    <p class="text-muted">
    <b>Remarque : </b>Lors de l'utilisation de **git push** une erreur s'affiche : <i>setsockopt IPV6_TCLASS 8: Operation not permitted:</i> mais il semble que ce soit bénin, et j'ai en effet pu modifier les fichiers sur gitlab à partir de ma copie locale (<a href="https://superuser.com/questions/1208370/whats-this-error-in-ssh-on-windows-subsystem-for-linux-setsockopt-ipv6-tclass">source</a>).
    </p>
  </div>
</div>

## 3 Construire le site

#### 3.1 Affichage local du site

Comme les modifications apportées à notre repository sur gitlab ne sont synchronisés que 2 fois par semaine avec notre site, il est important de pouvoir construire une version locale du site, en utilisant la copie locale du repository. Pour cela, on utilise **mkdocs** (on ne part pas de zéro, les fichiers présents sur gitlab sont déjà fait pour être utilisé par mkdocs).

```
$ mkdocs serve #permet d'afficher le site dans un navigateur
```

#### 3.2 Personnalisation du site
Pour personnaliser le site, il est possible d'en modifier le thème. Initialement, seuls les thèmes par défaut (mkdocs et Readthedocs) sont disponibles.
Pour installer de nouveaux thèmes, je suis passé par pip, avec les commandes suivantes
```
$ sudo apt-get install python3-pip

$ pip3 install mkdocs-windmill
$ pip3 install mkdocs-bootswatch
```

En regardant les thèmes proposés par bootswatch, j'ai commencé à regarder un peu plus en détail ce qu'il était possible de faire. Je me suis arrêté sur le thème _Darkly_ pour le confort de vos beaux yeux, et j'ai appris quelques commandes [html](https://www.w3schools.com/html/default.asp) pour ajouter des éléments dépassant les possibilités du Markdown. Par exemple, le cadre en haut de cette page utilise le code suivant :

```
<!-- Ceci est la balise html pour commentaires -->
<!-- Début du panneau (code adapté de l'exemple fourni par bootswatch pour le thème darkly) -->
<div class="jumbotron">
  <h1><u>Résumé de la semaine</u></h1>

  <hr class="my-4"> <!-- trace une ligne -->

  <h4>Objectifs</h4>
  <p class="lead">
  <ul> <!-- commencer une liste -->
    <li>Réfléchir à des <b>problématiques</b> nous tenant à cœur (les consigner sur la page final projet)
    <ul> <!-- sous liste -->
      <li>Pourquoi ça me tient à cœur</li>
      <li>poser le problème (<b>juste</b> le problème)</li>
      <li>rassembler des données sur le problème</li>
      </ul>
    </li>
    <li>Apprendre à utiliser les outils software nécessaire pour le cours (<b>Git</b>, <b>GitLab</b>, <b>Markdown</b>, <b>Bash</b>, <b>mkdocs</b>, etc.)</li>
    <li>Etablir la documentation de cette apprentissage</li>
  </ul>
  </p>

  <hr class="my-4"> <!-- trace une ligne -->

  <h4>Aller plus loin</h4>
  <p class="lead">
  <ul>
    <li>mieux comprendre le fonctionnement de linux et des commandes de base</li>
    <li>ne plus devoir entrer le mot de passe ssh à chaque push</li>
  </ul>
  </p>
</div>
<!--Fin du panneau -->
```

#### 3.3 Traitement d'images
Nous avons la possibilité d'inclure des images et vidéos sur le site, mais nous sommes limités en taille (et puis c'est dans l'esprit frugal de ne pas stocker une image 4k à laquelle personne ne fera vraiment attention sur un serveur outre-atlantique).

Pour un traitement rapide et efficace des images, j'utilise [ImageMagick](https://imagemagick.org/script/index.php), que j'installe en suivant [ces étapes](https://askubuntu.com/questions/745660/imagemagick-png-delegate-install-problems).

Voici les commandes de base qui permettent de réduire la qualité des images :

```
$ magick convert <filename.ext> <newExt>:<filename.ext> #converti un fichier en format <newExt>
# le nom après les ":" spécifie le nom du fichier output, en donnant le nom du fichier d'origine on l'écrase.

$ magick convert <fileName.ext> -quality 50% -resize 1000 <fileName.ext>
#change la qualité et la taille de l\'image.

$ magick mogrify -resize 800x800^^ -gravity center -extent 800x800 <fileName.ext>
#transforme l\'image en image 800x800

$ magick mogrify -geometry x400 <filename.ext>
# garde le ratio de l\'image en imposant 400 en vertical.

$ convert -append in-*.jpg out.jpg
# colle ensemble tous les fichiers jpg présent dans le dossier. -append : collage horizontal / +append : vertical
```
D'avantage de commandes peuvent être trouvées [ici](https://imagemagick.org/script/command-line-tools.php).
#### 3.4 Enregistrer son ecran sur Windows 10
windows+G

#### 3.5 coller deux video côte a côte
ffmpeg -i left.mp4 -i right.mp4 -filter_complex hstack output.mp4

#### 3.6 Inclure des vidéos

pour inclure une video on peut utiliser le code hmtl suivant :
```
<video controls>
<source src="../../images/5.Electronique/SOSMuted.mp4" type="video/mp4">
<!-- cette video est disponible dans le module de protoypage electronique -->
la phrase qui s'affiche si la video ne peut être affichée
</video>
```
<div class="card">
  <div class="card-body">
    <p class="text-muted">
    <b>Remarque : </b>Le chemin d'accès devrait commencer par "../images etc." mais pour que la vidéo fonctionne il faut écrire "../..images etc.", je ne sais pas pourquoi.
    </p>
  </div>
</div>

Pour diminuer la taille des fichiers, je retire la piste audio avec VLC ([tuto ici](https://www.techjunkie.com/remove-audio-from-video/)) (juste avant l'étape 5, ne pas oublier de sélectionner un "_profile_" avec piste video, VLC me propose par défaut un format .MP3)
