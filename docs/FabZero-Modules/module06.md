# 6. Electronique II - Fabrication

<div class="jumbotron">
  <h4>Objectifs</h4>
  <p class="lead">
  <ul>
    <li>Make a development board by stuffing a milled PCB (Printed Circuit Board), test it.</li>
    <li>Make your board do something</li>
  </ul>
  </p>
</div>

Le but du travail de cette semaine est de créer notre propre carte avec microcontrôleur, un équivalent simplifié de la carte Arduino Uno que nous avons utilisé la semaine passée.

Pour ça on nous a fourni une carte recouverte d'un film en cuivre, fraisée de façon à dessiner un circuit imprimé.
![image circuit imprimé vierge](../images/6.ElectroniqueFabrication/carteVierge.jpg)

## Composantes électroniques
Sur cette carte, on va venir souder les pièces suivantes :

  * un **microcontrôleur Atmel SAMD11C** : le cerveau de notre carte (l'équivalent de l'Atmega328p de l'Arduino Uno que nous avons utilisé la semaine passée). Ce microcontrôleur embarque nativement le protocole de communication USB, ce qui nous permet de brancher directement la carte sur un port USB de n'importe quel pc. Les microcontrôleurs que nous avons reçus contenait déjà le bootloader, afin de nous faciliter le travail et qu'on puisse se concentrer sur la soudure. Voici un schéma indiquant à quoi correspondent les différentes pattes de la puce (par exemple, la patte numéro 5 est estampillées "PA15", dans la programmation c'est donc via le numéro 15 qu'on y fera référence) :

  ![microcontrôleur schéma](../images/6.ElectroniqueFabrication/microcontroleur.jpg)

  * un **régulateur** qui transformera le courant 5 Volts de l'USB en courant 3.3 Volts requis pour le microcontrôleur.
  * un **capaciteur** pour assurer la stabilité du courant.

Remarque : avec ces 3 pièces, la carte est déjà fonctionnelle, on pourrait la brancher sur le port USB de son pc et communiquer avec

* Une **LED rouge** pour indiquer quand la carte est sous tension.
* Un **bouton**, qu'on pourra programmer pour recevoir l'input d'un utilisateur.
* Une **LED verte**, dont on se servira comme output (pour indiquer la réaction au bouton par exemple)
* **2x6 pins**, pour pouvoir venir brancher d'autres composantes électroniques (tous ceux qu'on a vu la semaine précédente par exemple).
* plusieurs **résistances** pour protéger le microcontrôleur et les LED.

##Soudure et programmation
Nous soudons les différentes pièces avec un alliage à base d'étain, contenant également une résine facilitant le mouillage du cuivre recouvrant la carte. Porter les fers à souder à 210°C est suffisant pour faire fondre le fil, et aller au-dessus ne sert à rien au contraire, ça accélère l'oxydation de la pointe du fer, ce qui le rend moins conducteur de chaleur. Cette oxydation a lieu aussi à 210°C (mais plus lentement) et il est donc nécessaire de régulièrement nettoyer la pointe, en la frottant sur une éponge humide. (On peut également limer légèrement la pointe mais c'est contre-indiqué par certains revendeurs, à vous de voir donc.)

### Le strict nécessaire :
J'ai commencé par souder le microcontrôleur, le régulateur et le capaciteur. C'était la première fois que je soudais et c'était pas évident au début, mais j'ai fini par prendre le coup de main.

![carte première étape](../images/6.ElectroniqueFabrication/carteBasique.jpg)

Avec ces 3 pièces soudées, j'ai pu brancher la carte sur mon pc. Mais sur cette carte, il n'y a rien qui permet d'indiquer qu'elle fonctionne ? Pour s'en assurer, on peut se rendre dans le _gestionnaire de périphériques_ (sous Windows), si un élément s'affiche dans la rubrique _Ports_ c'est sans doute la carte (dans le doute débranchez les autres périphériques connectés).

![gestionnaire périphériques](../images/6.ElectroniqueFabrication/carteDetectee.jpg)

### Une LED et un bouton :
Assuré du fonctionnement de la carte, j'ai ajouté une LED verte, avec une résistance, ainsi qu'un bouton.
![]()
J’ai pu installer un petit programme pour allumer la LED et vérifier qu'elle fonctionnait bien, mais je n'ai pas eu le temps de faire pareil pour le bouton à ce moment-là.
Pour installer des programmes sur la carte, on peut passer par l'IDE Arduino ! voici les paramètres utilisés (pour des explications détaillées de comment configurer l'IDE, je vous renvoie à la [documentation](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/dd760ea3b49b74d278b1eec1e4be9e5a45a986a6/Make-Your-Own-Arduino.md) de Nicolas) :

![Arduino param](../images/6.ElectroniqueFabrication/parametresArduino.jpg)

### Et le reste :
J'ai soudé les autres composantes un autre jour, plusieurs semaines plus tard. J'avais oublié la température de réglage du fer à souder mais heureusement j'ai trouvé mon bonheur dans la [super doc](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/FabZero-Modules/module06/) de Doriane. Malgré ça, et malgré mes tentatives pour nettoyer la pointe du fer, je n'arrivais à faire fondre le fil. J'ai été sauvé par Théo qui venait d'acheter son propre fer à souder.
Là j'ai fait une erreur, j'ai soudé toute la composante qui restait sans vérifier que la carte fonctionnait. Du coup quand je l'ai finalement branchée et que rien ne se passait, je n’avais pas l'air fin. Il y avait plusieurs courts circuits, que j'ai rectifié au fer et au cutter.
Finalement, la carte fonctionne, et j'ai pu tester le programme de Nicolas pour allumer et éteindre la LED verte en appuyant sur le bouton (en particulier, à chaque pression du bouton, l'état de la LED est inversé).
Les codes sont disponibles [sur mon GitLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/tree/master/docs/Arduino) et commencent par "HMB" (pour Home Made Board).

<video controls>
<source src="../../images/6.ElectroniqueFabrication/button.mp4" type="video/mp4">
</video>
