
# Module 2 : Conception Assistée par Ordinateur

<div class="jumbotron">
  <h4><u>Objectifs</u></h4>
  <p class="lead">
  <ul>
    <li>Se familiariser avec les logiciels OpenSCAD et FreeCAD</li>
    <li>Les utiliser pour reproduire des pièces <a href="https://www.compliantmechanisms.byu.edu/flexlinks">FlexLinks.</a></li>
    <li>Construire les pièces de façons paramétriques et construire la documentation pour rendre leur partage facile.</li>
  </ul>
  </p>

  <hr class="my-4"> <!-- trace une ligne -->

  <h4><u>Aller plus loin</u></h4>
  <p class="lead">
  <ul>
    <li>explorer plus de modules sur FreeCAD</li>
  </ul>
  </p>
</div>

## 1. Modélisation de FlexLinks sur OpenSCAD et FreeCAD

Parmi tous les [logiciels de modélisations](http://academy.cba.mit.edu/classes/computer_design/index.html) disponibles, deux nous ont été présentés en détails : OpenSCAD et FreeCAD, deux logiciels qui diffèrent par leur approche. D'un côté, OpenSCAD, un logiciel ou l'utilisateur décrit (à l'aide d'équations et d'un langage de programmation sommaire) la pièce qu'il souhaite modéliser. De l'autre, FreeCAD, un logiciel basé sur le dessin de la pièce par l'utilisateur. Ces deux logiciels peuvent être utilisés de concert, une pièce crée avec OpenSCAD peut être importée et modifiée dans FreeCAD.

#### 1.1 OpenSCAD
J'ai commencé par tester OpenSCAD, en essayant de reproduire le FlexLinks "Cantilever Beam" (_poutre en porte-à-faux_), il avait l'air assez simple et ça m'a fait penser à mon mémoire sur les abeilles.

<div class="card">
  <div class="card-body">
    <p class="text-muted">
    <b>le rapport avec les abeilles ?</b> Et bien les abeilles se nourrissent de nectar, elles doivent donc pouvoir le récupérer dans les fleurs.
    Mais contrairement à nous, elles ne sont pas capables d'aspirer de liquide, c'est morphologique, même si on leurs donnait des mini pailles, elles ne sauraient rien en faire.
    Pour capturer du nectar, une abeille trempe sa glosse (ce que j'ai toujours envie d'appeler une langue) dedans. Comme la plupart des objets qui plongent dans un liquide, la glosse ressort du nectar en emportant un fin film avec elle. Cet effet capillaire est démultiplié grâce aux cils qui couvrent la glosse des abeilles et agissent un peu comme les poils d'un pinceau. Lors du processus, ces cils sont tantôt plaqués contre la glosse, tantôt ouverts. C'est cette dynamique de mouvement de tige flexible qui m'a fait penser au Cantilever Beam. Comment ? vous trouvez ça capillotracté ?
    Vous pouvez vous faire une meilleure idée avec <a href="https://www.youtube.com/watch?v=VFIMR6bWwkE">cette vidéo</a>.
    </p>
  </div>
</div>

![image](../images/2.CAD/OpenSCAD.jpg "OpenSCAD - Cantilever Beam")

Faire un FlexLinks peut sembler une tâche compliquée quand on débute, je recommande de commencer par créer une seule forme simple (un cube ou cylindre par exemple), de jouer un peu avec en changeant ses dimensions, son orientation, sa position. Puis en créer une seconde et voir tout ce qu'on peut faire avec deux pièces, comme en prendre l'enveloppe ou la différence. Il existe une magnifique [CheatSheet](http://openscad.org/cheatsheet/) qui reprend les fonctions dont vous aurez besoins (avec des liens vers le wiki pour chacune d'entre elles).

Le fichier .scad (modifiable dans OpenSCAD) est trouvable [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/blob/master/docs/CADFiles/cantileverBeam.scad), le fichier .stl (avec prévisualisation intégrée à GitLab) [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/blob/master/docs/CADFiles/cantileverBeam.stl).

Utiliser OpenSCAD requiert donc une compréhension totale de la géométrie de la pièce qu'on souhaite créer. Ça peut être difficile, mais une fois qu'on y parvient on gagne un contrôle égal sur notre pièce.

Voici le code pour cette pièce, avec quelques commentaires sur son fonctionnement :
```
/*
    FILE    : cantileverBeam.scad

    AUTHOR  : Paul Bryssinck <paulbryssinck@hotmail.com>

    DATE    : 2021-05-05

    LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0)
    https://creativecommons.org/licenses/by/4.0/

    Original design by the BYU Compliant Mechanisms Research Group (CMR)
    https://www.compliantmechanisms.byu.edu/flexlinks
*/

//parametres
$fn=100; //il est important d'avoir un nombre de faces assez élevé pour que les trous soient lisses
         //(sinon ils ne se clipseront pas sur les Lego).
nHoles=3; //nombre de trous par fixation (détermine aussi la taille de la fixation)
thick=3; //l'epaisseur de la fixation (direction z)
rOut=4;  //la largeur de la fixation (direction y)
         //la longueur de la fixation (direction x) est nHoles*2*rOut
rInn=2.5; //le rayon des trous
linkLen=70; //la longueur de la tige (direction x)
linkWid=1.5; //l'epaisseur de la tige (direction y)
             //la largeur de la tige (direction z) vaut rOut
holeSep=0; //controle l'espacement entre les trous, pas necessaire pour le standard Lego


/*
  coeur de la fixation :

    prend l'enveloppe (fonction "hull") de deux cylindre, de rayon rOut, qu'on espace (fonction "translate")
    en fonction du nombre de trous qu'on fera dedans.
*/
module filledClip(){
    hull(){cylinder(h=thick, r=rOut);
translate([2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0])cylinder(h=thick, r=rOut);}
}

/*
  cantilever (la tige) :

    la tige est formée de l'enveloppe de deux cylindres, qu'on tourne et déplace de façon à avoir une extremité
    dans la fixation (pour être precis, le bout de la tige arrive pile au centre du trou le plus à droite
    de la fixation (comme on creuse le trou apres avoir placé la tige c'est pas un probleme)).
*/
module cantilver(){
    dist=(nHoles-1)*(2*rOut+holeSep); //la position x du début de la tige (dans la fixation)
hull(){
translate([dist,linkWid/2,thick/2])rotate([90,0,0])cylinder(r=thick/2,h=linkWid);
translate([dist+linkLen,linkWid/2,thick/2])rotate([90,0,0])cylinder(r=thick/2,h=linkWid);}
}

/*
  Assemblage et perçage de la pièce :

    la fonction difference prend son premier argument (ici l'union de la fixation et de la tige) et
    en retire le second argument (ici les trous, défini comme des cylindres dans la boucle for).
*/
module assembly(){
    difference(){union(){cantilver();filledClip();}
    for(i=[1:nHoles]){
        translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick,r=rInn);}
    }
}

//On cree la piece, qu'on déplace pour que le début de la tige soit centree en l'origine
//c'est juste visuel, on pourrait simplement appeller "assembly();".
translate([-(nHoles)*(2*rOut+holeSep)+rOut+holeSep,0,0])assembly();

```

#### 1.2 FreeCAD
J'ai ensuite reproduit une seconde pièce, en utilisant FreeCAD cette fois.
Pour créer une pièce, je commence par dessiner en deux dimension un schéma, que j'étire dans la 3e dimension ensuite. Je me retrouve alors avec une pièce pleine, sur laquelle je peux redessiner un schéma pour indiquer où creuser la pièce.

Ces différentes étapes sont illustrées avec la pièce crossAxis-FlexuralPivot ( fichiers [.FCStd](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/blob/master/docs/CADFiles/WRONGCrossAxis-FlexuralPivot.FCStd) et [.stl](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/blob/master/docs/CADFiles/WRONGCrossAxis-FlexuralPivot.stl)) :
![les 4 étapes de créations d'une pièce sur FreeCAD](../images/2.CAD/freeSteps.jpg "Étapes de création du Cross-Axis Flexural Pivot ") le nom des fichiers commence par "WRONG" car je n'avais pas vu que le "X" qui relie les deux attaches est constitué de deux couches séparées dans le design original.)

La première pièce que j'ai créé sur FreeCAD est le Fixed-Fixed Beam (fichiers [.FCStd](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/blob/master/docs/CADFiles/FFBeam.FCStd) et [.stl](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/blob/master/docs/CADFiles/FFBeam.stl)):

![image2](../images/2.CAD/FreeCAD.jpg "FreeCAD - Fixed-Fixed Beam")

#### 1.3 Comparaison des logiciels

De prime abord, FreeCAD est plus simple à utiliser, l'interface est assez intuitive, et on voit en temps réel notre pièce se construire. Au contraire, OpenSCAD peut être frustrant tant qu'on n'a pas entièrement compris la pièce. Mais passé les premières difficultés sur OpenSCAD, ça devient très facile à utiliser, et j'ai pu rapidement reproduire plusieurs pièces aux géométries proches. Je pense cependant qu'OpenSCAD est d'autant plus dur à utiliser que la pièce est complexe, il est sûrement intéressant de pouvoir combiner les deux logiciels pour allier la rapidité d'OpenSCAD à la finesse de FreeCAD.

J'ai continué à m'entrainer sur les deux logiciels pour constituer un kit à destination du futur SteamLab, composé des FlexLinks suivants :

* Airplane
* Cross-axis flexural pivot
* Fixed-fixed beam
* Fixed-fixed beam - Half-circle (en FreeCAD et OpenSCAD, je suis curieux de voir la différence à l'impression)
* Fixed-fixed beam - Out of plane
* Fixed beam - Initially curved
* Fixed-slotted beam - Straight
* Satellite

Toutes ces pièces sont disponibles dans le dossier _CAD Files_ sur mon [repository GitLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck) et paramétrées, il devrait être facile de les modifier au besoin.


## 2. Adaptation au standard Lego

Maintenant qu'on peut faire des pièces, et comme toutes les grandeurs sont paramétrisées, je peux facilement les adapter pour utiliser les FlexLinks avec des Lego. Stéphanie a fait des recherches sur les standards Lego (disponible sur [son site](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/FabZero-Modules/module02)) et des tests. D'après ses résultats, bien que la taille annoncée des attaches soit de 4.8 mm de diamètre, en pratique c'est avec des trous de 5 mm que les FlexLinks s'attachent correctement. L’espacement entre deux trous voisins doit être de 8 mm.

La table suivante reprend les grandeurs adéquates qui se retrouvent dans la plupart des FlexLinks que j'ai reproduit.

<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Nom de la grandeur</th>
      <th scope="col">Taille (mm)</th>
      <th scope="col">Rôle de la grandeur</th>
    </tr>
  </thead>
  <tbody>
    <tr class="table-dark">  <!-- thick -->
      <td style="text-align: center" scope="row">thick</td>
      <td style="text-align: center" >3</td>
      <td>épaisseur du FlexLinks (pour les modèles créés sur FreeCAD, est notamment utilisée lors de l'extrusion du schéma 2D à l'objet 3D).</td>
    </tr>
    <tr class="table-dark">  <!-- rOut -->
      <td style="text-align: center" scope="row">rOut</td>
      <td style="text-align: center" >4</td>
      <td>rayon extérieur des attaches. L’espacement entre le centre de deux trous voisins est le double de cette valeur.</td>
    </tr>
    <tr class="table-dark"> <!-- rInn -->
      <td style="text-align: center" scope="row">rInn</td>
      <td style="text-align: center" >2.5</td>
      <td>rayon des trous dans les attaches (1 mm de plus que celui annoncé dans la doc Lego).</td>
    </tr>
    <tr class="table-dark">  <!-- linkWid -->
      <td style="text-align: center" scope="row">linkWid</td>
      <td style="text-align: center" >1.5</td>
      <td>largeur des liens flexibles entre les attaches. Une valeur plus grande diminue la flexibilité de la pièce.</td>
    </tr>
  </tbody>
</table>
