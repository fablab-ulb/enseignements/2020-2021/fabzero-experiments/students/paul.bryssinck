## Welcome to my personnal FabZero-Experiments website

- This website's purpose is to keep track of my progress in the FabZero Experiments (PHYS-F517 - How To Make (almost) Any Experiment Using Digital Fabrication).
- it is built using [Mkdocs](https://mkdocs.org) a static site generator written in Python
